﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Guitar : Interactable
{
    protected override void decideOnInteraction(Collider other)
    {
        var ap = GetComponent<AudioSource>();
        //base.decideOnInteraction(other);

        if (!opened)
        {
            if (other.tag == "Player")
            {
                ap.Play();
            }
        }
    }

    protected override string hintText()
    {
        return "Play " + Name;
    }

}

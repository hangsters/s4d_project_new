﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRotation : MonoBehaviour
{
    public GameObject focusItem;
    public float rotationSpeed = 10;

    public float minFov = 15f;
    public float maxFov = 90f;
    public float sensitivity = 10f;

    private void rotateCamera()
    {
        transform.RotateAround(focusItem.transform.position, transform.up, -Input.GetAxis("Mouse X") * rotationSpeed);
        transform.RotateAround(focusItem.transform.position, transform.right, -Input.GetAxis("Mouse Y") * rotationSpeed);
    }
    private void Update()
    {
        if (Input.GetMouseButton(0)) {
            rotateCamera();
        }
        float fov = Camera.main.fieldOfView;
        fov -= Input.GetAxis("Mouse ScrollWheel") * sensitivity;
        fov = Mathf.Clamp(fov, minFov, maxFov);
        Camera.main.fieldOfView = fov;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mobiel : Interactable
{
    private int interactCounter = 0;
    public bool answered = false;
    public AudioClip Hangupsound;

    protected override void Pressed()
    {
        ChangeState();
    }
    private void ChangeState()
    {
        var ap = GetComponent<AudioSource>();

        if (interactCounter == 0) 
        {
            ap.Play();
            interactCounter++;
            answered = true;
        }
        else
        {
            ap.Stop();
            ap.PlayOneShot(Hangupsound);
            answered = false;
        }
    }

    protected override string hintText()
    {
        if (answered == false)
        {
            return "Answer " + Name;
        }
        else 
        {
            return "Hang up " + Name;
        }
    }
}
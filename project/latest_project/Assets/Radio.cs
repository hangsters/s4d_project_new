﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radio : Interactable
{
    protected override void Pressed()
    {
        ChangeState();
    }
    private void ChangeState()
    {
        var ap = GetComponent<AudioSource>();

        if (ap.isPlaying)
        {
            ap.Pause();
        }
        else
        {
            ap.Play();
        }
    }

    protected override string hintText()
    {
        return "Power " + Name;
    }


}

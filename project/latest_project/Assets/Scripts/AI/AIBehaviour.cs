﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIBehaviour : MonoBehaviour
{
    public NavMeshAgent agent;

    public Transform player;

    public GameObject playerComp;
    public LayerMask whatIsGround, whatIsPlayer;
    public LootTable thisLoot;

    public float health;

    public GameObject damageNum;

    [SerializeField]
    public Transform SpawnRock;
    
    //Patroling
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    //Attacking
    public float timeBetweenAttacks;
    protected bool alreadyAttacked;
    public GameObject projectile;

    //States
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange , playerInHearingRange, playerInFOV;

    private void Awake()
    {
        player = GameObject.Find("Player").transform;
        playerComp = GameObject.Find("Player");
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        //Check for sight and attack range
        playerInSightRange = Physics.CheckSphere(transform.position, sightRange, whatIsPlayer);
        playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, whatIsPlayer);

        
        if (playerInSightRange && !playerInAttackRange || playerInHearingRange || playerInFOV) ChasePlayer();
        else if (playerInAttackRange && playerInSightRange) AttackPlayer();
        else Patroling();
    }

    private void Patroling()
    {
        if (!walkPointSet) SearchWalkPoint();

        if (walkPointSet)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        //Walkpoint reached
        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }
    private void SearchWalkPoint()
    {
        //Calculate random point in range
        float randomZ = Random.Range(-walkPointRange, walkPointRange);
        float randomX = Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, whatIsGround))
            walkPointSet = true;
    }

    public void ChasePlayer()
    {
        agent.SetDestination(player.position);
    }

    protected virtual void AttackPlayer()
    {
        //Make sure enemy doesn't move
        var ap = GetComponent<AudioSource>();
        agent.SetDestination(transform.position);

        transform.LookAt(player);

        if (!alreadyAttacked)
        {
            ///Attack code here
            ap.pitch = UnityEngine.Random.Range(0.8f, 1.1f);
            ap.Play();
            Rigidbody rb = Instantiate(projectile, SpawnRock.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 25f, ForceMode.Impulse);
            rb.AddForce(transform.up * 1f, ForceMode.Impulse);
            ///End of attack code

            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }
    protected void ResetAttack()
    {
        alreadyAttacked = false;
    }

    public void TakeDamage(float damage)
    {
        health -= Mathf.RoundToInt(damage);
        showDamage(Mathf.RoundToInt(damage));
        if (health <= 0) Invoke(nameof(DestroyEnemy), 0.5f);
    }

    private void showDamage(int damage)
    {
        GameObject DamagePopup = Instantiate(damageNum, transform.position + new Vector3(Random.Range(-1, 1), 2, 0.5f), Quaternion.Euler(90, 0, 0));
        damagePopup damagetext = DamagePopup.GetComponent<damagePopup>();
        damagetext.setup(damage);
    }
    private void DestroyEnemy()
    {
        makeLoot();
        Destroy(this.gameObject);
    }

    private void makeLoot()
    {
        if(thisLoot != null)
        {
            Item current = thisLoot.LootItem();
            if(current != null)
            {
                Instantiate(current.thisObject, transform.position, Quaternion.identity);
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, sightRange);
    }
}

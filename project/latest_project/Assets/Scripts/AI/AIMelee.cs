﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIMelee : AIBehaviour
{
    protected override void AttackPlayer()
    {
        var ap = GetComponent<AudioSource>();
        //Make sure enemy doesn't move
        agent.SetDestination(transform.position);

        transform.LookAt(player);

        if (!alreadyAttacked)
        {
            ///Attack code here
            if (isHit())
            {
                ap.pitch = UnityEngine.Random.Range(0.8f, 1.1f);
                ap.Play();
                Debug.Log("hit");
                playerComp.GetComponent<PlayerHealthManagement>().TakeDamage(10);
            }
            //Rigidbody rb = Instantiate(projectile, transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            //rb.AddForce(transform.forward * 32f, ForceMode.Impulse);
            //rb.AddForce(transform.up * 8f, ForceMode.Impulse);
            ///End of attack code

            alreadyAttacked = true;
            Invoke(nameof(ResetAttack), timeBetweenAttacks);
        }
    }

    protected bool isHit()
    {
        for(int i = 0; i <= 8; i++)
        {
            float angle = 90 - i * (90 * 2 / 8) - Mathf.Abs(transform.rotation.eulerAngles.y + 270);
            Vector3 direction = transform.forward + new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), 0, Mathf.Sin(angle * Mathf.Deg2Rad));
            Ray ray = new Ray(transform.position, direction);
            if(Physics.Raycast(ray, out RaycastHit hitInfo, attackRange))
            {
                return true;
            }
        }
        return false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionAI : MonoBehaviour
{
    public GameObject explosionEffect;
   
    public void Explode()
    {
        var ap = GetComponent<AudioSource>();
        showEffect();
        ap.Play();
    }

    private void showEffect()
    {
        Instantiate(explosionEffect, transform.position + new Vector3(0, 1, 0), Quaternion.identity);
    }
}

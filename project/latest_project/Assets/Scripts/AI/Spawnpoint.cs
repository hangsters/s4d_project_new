﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawnpoint : MonoBehaviour
{
    [SerializeField] private GameObject[] AI;
    [SerializeField] private GameObject[] spawnlocations;

    private int numSpawnLocations;
    private int numAIElements;

    private float waitTimeSpawnAI = 10.0f;
    private float currentTime;
    private int counter = 0;

    // Start is called before the first frame update
    void Start()
    {
        numSpawnLocations = countList(spawnlocations);
        numAIElements = countList(AI);
    }

    // Update is called once per frame
    void Update()
    {
        // timer
        if (currentTime == 0)
        {
            spawnAI(AI, spawnlocations);
            counter++;
        }

        if (currentTime < waitTimeSpawnAI)
        {
            currentTime += 1 * Time.deltaTime;
        }

        if (currentTime >= waitTimeSpawnAI)
        {
            currentTime = 0;
        }
        // timer stop
    }

    private int countList(GameObject[] spawnlocations)
    {
        return spawnlocations.Length;
    }

    // Random.Range(min, max), max is value - 1
    private int randomValue(int min, int max)
    {
        return Random.Range(min, max);
    }

    private void spawnAI(GameObject[] AIelements, GameObject[] spawnlocations)
    {
        int indexSpawnLocation = randomValue(0, numSpawnLocations);
        int indexAI = randomValue(0, numAIElements);

        GameObject.Instantiate(AIelements[indexAI], spawnlocations[indexSpawnLocation].transform.position * 1.05f, Quaternion.identity, this.transform);
    }
}

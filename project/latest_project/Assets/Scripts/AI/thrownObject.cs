﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class thrownObject : MonoBehaviour
{
    public float lifeTime;

    [SerializeField]
    private int damage;
    
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            Destroy(this.gameObject);
        }
    }

    private int counter;
    private void OnCollisionEnter(Collision collision)
    {
        // Debug.Log("col");
        if (collision.collider.CompareTag("Player"))
        {
            // Debug.Log("hit rock");
            if (counter % 2 == 0)
                TakeDamagePlayer(damage);
        }

        counter++;
    }

    private void TakeDamagePlayer(float damage)
    {
        PlayerHealthManagement.MyInstance.TakeDamage(damage);
    }
}

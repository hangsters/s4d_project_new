﻿////////////////Crafting.cs//////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crafting : MonoBehaviour
{
    public List<GameObject> inputSlots;
    public List<GameObject> outputSlots;
    public List<CraftingOutput> craftOutputs;

    public List<SORecipe> craftingRecipes;
    public List<SORecipe> possibleRecipes = new List<SORecipe>();

    public List<PlayerClass> currentClass;
    public List<Item> input = new List<Item>();

    // Start is called before the first frame update.
    void Start()
    {
        List<SORecipe> additionalRecipes = currentClass[PlayerPrefs.GetInt("PlayerClass")].classRecipes;
        craftingRecipes.AddRange(additionalRecipes);
    }
    private void OnEnable()
    {
        CraftingOutput.onOutputClick += checkIfItemIsUsed;
    }

    private void OnDisable()
    {
        CraftingOutput.onOutputClick -= checkIfItemIsUsed;
    }
    public void DoeDingen()
    {
        input.Clear();
        for (int i = 0; i < inputSlots.Count; i++)
        {
            SlotScript craftInput = inputSlots[i].GetComponent<SlotScript>();
            if (craftInput.MyItem != null)
            {
                input.Add(craftInput.MyItem);
            }
        }
        checkRecipe();
    }

    private void checkRecipe()
    {
        possibleRecipes.Clear();
        if(input.Count != 0)
        {

        }
        for (int i = 0; i < craftingRecipes.Count; i++)
        {
            if (isValidInput(i))
            {
                possibleRecipes.Add(craftingRecipes[i]);
            }
        }
        generateOutput();
    }

    private void generateOutput()
    {
        for(int i = 0; i < possibleRecipes.Count; i++)
        {
            craftOutputs[i].showOutput(possibleRecipes[i]);
        }
    }

    private bool isValidInput(int i)
    {
        for (int index = 0; index < craftingRecipes[i].recipe.Count; index++)
        {
                if (notInInput(i, index))
                {                    
                    return false;
                }            
        }
        return true;
    }

    private bool notInInput(int i, int index)
    {
        foreach (Item item in input)
        {
            if(item.Name == craftingRecipes[i].recipe[index].Name)
            {
                return false;
            }
        }
        return true;
    }

    private void checkIfItemIsUsed(SORecipe Recipe)
    {
        if (Recipe != null)
        {
            for(int index = 0; index < inputSlots.Count; index++)
            {
                SlotScript craftInput = inputSlots[index].GetComponent<SlotScript>();
                for (int i = 0; i < Recipe.recipe.Count; i++)
                {
                    if (craftInput.MyItem != null)
                    { 
                        if (Recipe.recipe[i].Name == craftInput.MyItem.Name)
                        {
                            craftInput.RemoveItem(craftInput.MyItem);
                        }
                    }
                }                                                       
            }            
        }
    }
}

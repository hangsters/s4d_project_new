﻿////////////////CraftingInput.cs//////////////////////

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingInput : MonoBehaviour
{
    public CraftingItem item = null;

    private Image iconImage;

    public Sprite slotImage;

    private void Awake()
    {
        iconImage = GetComponent<Image>();
    }

    private void OnEnable()
    {
        CraftingOutput.onOutputClick += checkIfItemIsUsed;
    }

    private void OnDisable()
    {
        CraftingOutput.onOutputClick -= checkIfItemIsUsed;
    }

    public void updateItem(CraftingItem currentItem)
    {
        item = currentItem;
    }

    private void checkIfItemIsUsed(SORecipe recipe)
    {
        if (recipe != null)
        {
            for (int i = 0; i < recipe.recipe.Count; i++)
            {
                if (recipe.recipe[i] == item)
                {
                    item = null;
                    iconImage.sprite = slotImage;
                }
            }
        }

        /*
        public void changeInput(LootableObject inputItem)
        {
            item = inputItem;
        }
        */

    }
}


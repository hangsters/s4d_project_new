﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CraftingOutput : MonoBehaviour
{
    private Image icon;
    private Item outputItem;
    private SORecipe currentRecipe = null;

    public delegate void MyDelegate(SORecipe recipe);
    public static event MyDelegate onOutputClick;
    public Sprite SlotImage;

    

    private void Awake()
    {
        icon = GetComponent<Image>();
    }

    public void showOutput(SORecipe recipe)
    {
        currentRecipe = recipe;
        icon.sprite = recipe.outputObject.MyIcon;
        outputItem = recipe.outputObject;
    }

    public void onClick()
    {
        string itemType = outputItem.GetType().ToString();
        if (itemType == AllItems.CraftingItem.ToString())
        {
            CraftingItem output = (CraftingItem)Instantiate(outputItem);
            InventoryScript.MyInstance.AddItem(output);
        }
        onOutputClick(currentRecipe);
        outputItem = null;
        icon.sprite = SlotImage;
        currentRecipe = null;
    }

    enum AllItems
    {
        HealthPotion = 0,
        Bag = 1,
        CraftingItem = 2,
    }
}

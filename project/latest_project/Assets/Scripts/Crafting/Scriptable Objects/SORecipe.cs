﻿////////////////SORecipe.cs//////////////////////
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class SORecipe : ScriptableObject
{
    public List<Item> recipe;
    public Item outputObject;
}

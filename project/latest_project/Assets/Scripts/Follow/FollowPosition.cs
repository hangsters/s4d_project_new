﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPosition : MonoBehaviour
{
    public Transform Player;
    public Vector3 Offset = new Vector3(1, 0.5f, 1);

    // Update is called once per frame
    void LateUpdate()
    {
        transform.position = Player.position + Offset;
    }
}

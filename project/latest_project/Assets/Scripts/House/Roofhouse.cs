﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Roofhouse : MonoBehaviour
{

    public GameObject Roof = null;

    void Start()
    {

    }

    private void OnTriggerStay(Collider collider)
    {
        if (collider.tag == "Player")
        {
            Roof.SetActive(false);
        }
    }

    private void OnTriggerExit(Collider collider)
    {
        if (collider.tag == "Player")
        {
            Roof.SetActive(true);
        }

    }


    // Update is called once per frame
    void Update()
    {
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeObject : Interactable
{

    // Storing different levels'
    public GameObject[] levels;

    private bool searchForKeyDown = false;
    private bool upgraded = false;
    // Counting current level
    int current_level = 0;

    public void Update()

    {
        if (searchForKeyDown)
        {
            Debug.Log("Key is ready om gedrukt te worden");
            if (Input.GetKeyDown(KeyCode.E))
            {
                Pressed();
                Debug.Log("You Pressed E");

                //Delete if you dont want Text in the Console saying that You Press F.

            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (!upgraded)
        {
            if (other.tag == "Player")
            {
                searchForKeyDown = true;
            }
        }
        print("Press E to barricade Window.");
    }

    public void CheckLevel()
    { 
        if (current_level < levels.Length - 1)
        {
            // Increase current level
            current_level++;
            // Switch to the updated level
            SwitchObject(current_level);
        }
    }

    public void SwitchObject(int lvl)
    {
        // Count from zero the last level in our array
        for (int i = 0; i < levels.Length; i++)
        {
            // Check if we're in the desired level, then activate
            if (i == lvl)
                levels[i].SetActive(true);
            else
                //Otherwise, hdie it
                levels[i].SetActive(false);
        }
    }
    protected override void Pressed()
    {
        //This will set the bool the opposite of what it is.
        upgraded = !upgraded;
        CheckLevel();
    }
    protected override string hintText()
    {
        return "Barricade " + Name;
    }
}
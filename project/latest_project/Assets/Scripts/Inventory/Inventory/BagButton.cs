﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class BagButton : MonoBehaviour, IPointerClickHandler
{
    // if this bag exists or it is null, there is a bag equiped on this bagbutton
    private Bag bag;

    // Which bag is equiped
    [SerializeField] private Sprite full, empty;

    public Bag MyBag
    {
        get
        {
            return bag;
        }

        set
        {
            if (value != null)
            {
                GetComponent<Image>().sprite = full;
            }
            else
            {
                GetComponent<Image>().sprite = empty;
            }

            bag = value;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        // if we have a bag attach to the bag button we would like to open or close it
        if (bag != null)
        {
            bag.MyBagScript.OpenClose();
        }
    }
}

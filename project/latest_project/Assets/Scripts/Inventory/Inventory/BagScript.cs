﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BagScript : MonoBehaviour
{
    /// <summary>
    /// Prefab for creating slots
    /// </summary>
    [SerializeField] private GameObject slotPrefab;
    
    /// <summary>
    /// A canvasgroup for hiding and showing the bag
    /// </summary>
    private CanvasGroup canvasGroup;

    public List<SlotScript> Slots = new List<SlotScript>();

    /// <summary>
    /// Indicates if the bag is open or closed
    /// </summary>
    public bool IsOpen
    {
        get
        {
            return canvasGroup.alpha > 0;
        }
    }

    public List<SlotScript> MySlots
    {
        get
        {
            return Slots;
        }
    }


    private void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    /// <summary>
    /// Creates slots for this bag
    /// </summary>
    /// <param name="slotCount"></param>
    public void AddSlots(int slotCount)
    {
        for (int i = 0; i < slotCount; i++)
        {
            SlotScript slot = Instantiate(slotPrefab, transform).GetComponent<SlotScript>();
            Slots.Add(slot);
        }
    }

    public bool AddItem(Item item)
    {
        foreach (SlotScript slot in Slots)
        {
            if (slot.IsEmpty)
            {
                slot.AddItem(item);
                InventoryScript.MyInstance.items.Add(item); // 01

                return true;
            }
        }

        return false;
    }
    
    /// <summary>
    /// Open or closes bag
    /// </summary>
    public void OpenClose()
    {
        canvasGroup.alpha = canvasGroup.alpha > 0 ? 0 : 1;

        canvasGroup.blocksRaycasts = canvasGroup.blocksRaycasts == true ? false : true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Opmerkingen
// the slotSize is afhankelijk van het character, elk character heeft een eigen Scriptable Object
// Deze door middel van een [SerializeField] met een base class laten ingeven en in de awake method laten uitlezen

// -----------------------------------------------------------------------------

// This is going to be the top script, the one that manages the other scripts
// like the bag and everything

// handigheidjes
// wanneer je gameobjecten moet implementeren de [SerializeField] optie gebruiken
// Bij declaring van de fields alleen bijvoorbeeld public int Age; doen, niet public int Age = 10; dat laatste moet in de Awake method


public class InventoryScript : MonoBehaviour
{
    private static InventoryScript instance;

    // Check if only one GameObject have this script added to
    // singleton
    public static InventoryScript MyInstance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<InventoryScript>();
            }

            return instance;
        }
    }

    // ----------------------------------------------------------

    // List of all the bags in our inventory
    [HideInInspector] public List<Bag> Bags = new List<Bag>();

    /// <summary>
    /// Input Scriptable object for creating the bag in the Awake method
    /// </summary>
    [SerializeField] private Item bagObject;
    [HideInInspector] public int itemCount;

    private Bag bag;                                // for debugging purposes if slotsize is null
    public float SpeedModifier;

    // Based on this we can just instantiate whatever item we would like to test in our inventory
    // Normal in the gameplay there will be loottables 
    // This is just for debugging
    [SerializeField] public List<Item> items = new List<Item>();
    
    /// <summary>
    /// Will be generated from the PlayerController script
    /// </summary>
    [HideInInspector] public int SlotSize;                              
    
    /// <summary>
    /// List of all the bag buttons -> currently 1 in project
    /// </summary>
    [SerializeField] private BagButton[] bagButtons;

    
    /// <summary>
    /// When user pick something from inventoryUI slot, this is the selected slot
    /// </summary>
    private SlotScript fromSlot;

    private void Start()
    {
        // Generate the object
        bag = (Bag)Instantiate(bagObject);       // In general this is not good practice cause there are lots of different items in the items field. // ?
                                                 // now we only take item 0
        
        // For local purposes
        if (SlotSize == 0)
            bag.Initialize(15);
        else
            bag.Initialize(SlotSize);

        bag.Use();                              // Create the bag and create the slots

        OpenClose();                            // When game starts, inventory is closed
    }

    private void Update()
    {
        // Only for debugging
        //if (Input.GetKeyDown(KeyCode.I))
        //{
        //    Bag bag = (Bag)Instantiate(items[0]);   // Makes a copy of the bag (item[0]) asset (Scriptable Object)
        //    bag.Initialize(slotSize);
        //    bag.Use();                              // belangrijke method
        //}
        //if (Input.GetKeyDown(KeyCode.K))
        //{
        //    Bag bag = (Bag)Instantiate(items[0]);  
        //    bag.Initialize(slotSize);
        //    AddItem(bag);
        //}
        //if (Input.GetKeyDown(KeyCode.L))
        //{
        //    HealthPotion potion = (HealthPotion)Instantiate(items[0]);
        //    AddItem(potion);
        //}
        //if (Input.GetKeyDown(KeyCode.H))
        //{
        //    AddItem((Armor)Instantiate(items[1]));
        //    AddItem((Armor)Instantiate(items[2]));
        //    AddItem((Armor)Instantiate(items[3]));
        //    AddItem((Armor)Instantiate(items[4]));
        //    AddItem((Armor)Instantiate(items[5]));
        //    AddItem((Weapon)Instantiate(items[6]));
        //    AddItem((Weapon)Instantiate(items[7]));
        //    AddItem((Weapon)Instantiate(items[8]));
        //}

        // This will be define in the UIManager class
        //if (Input.GetKeyDown(KeyCode.I))
        //{
        //    OpenClose();
        //}


        //if (Input.GetKeyDown(KeyCode.M))
        //    print("Speedmodifier: " + SpeedModifier);

        // The speedmodifier must be constantly updated
        SpeedModifier = CalculateSpeedModifierInventory(bag.MyBagScript.MySlots.Count);

        // Count the number of free slots in the inventory
        //print("count of used slots " + UsedSlots(Bags));
    }

    /// <summary>
    /// Add bagbuttons for the bag bar at the bottom
    /// </summary>
    public bool CanAddBag
    {
        get { return Bags.Count < 1; }
    }

    /// <summary>
    /// property to check if the player can add an item to the inventory. 
    /// </summary>
    public bool CanAddItem
    {
        get { return UsedSlots(Bags) < bag.MyBagScript.MySlots.Count; }
    }

    public SlotScript FromSlot
    {
        get
        {
            return fromSlot;
        }

        set
        {
            fromSlot = value;

            if (value != null)
            {
                fromSlot.MyIcon.color = Color.grey;
            }
        }
    }

    public void AddBag(Bag bag)
    {
        foreach (BagButton button in bagButtons)
        {
            if (button.MyBag == null) // if i don't have equip a back already
            {
                // equip back
                button.MyBag = bag;
                Bags.Add(bag);
                break;
            }
        }
    }

    /// <summary>
    /// Adds an item to the inventory
    /// </summary>
    /// <param name="item"></param>
    public void AddItem(Item item)
    {
        if (item.MyStackSize > 0)
        {
            if (PlaceInStack(item))
            {
                return;
            }
        }

        PlaceInEmpty(item);
    }

    private void PlaceInEmpty(Item item)
    {
        foreach (Bag bag in Bags)
        {
            if (bag.MyBagScript.AddItem(item))
            {
                itemCount++;
                return;
            }
        }
    }

    private bool PlaceInStack(Item item)
    {
        foreach(Bag bag in Bags)
        {
            foreach(SlotScript slots in bag.MyBagScript.MySlots)
            {
                if (slots.StackItem(item))
                {
                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Opens and closes all bags
    /// </summary>
    public void OpenClose()
    {
        // looks in all the bags in the bags list, if we find a single bag 
        // that is closed than we set closedBag to true
        bool closedBag = Bags.Find(x => !x.MyBagScript.IsOpen);

        // IF closed bag == false, then close all open bags
        // IF closed bag == true, then open all open bags

        foreach (Bag bag in Bags)
        {
            if (bag.MyBagScript.IsOpen != closedBag)
            {
                bag.MyBagScript.OpenClose();
            }
        }
    }

    /// <summary>
    /// Calculate the total weight of the inventory and run this method in the Update function
    /// </summary>
    /// <returns></returns>
    public int CalculateCurrentTotalWeight()
    {
        int totalWeight = 0;

        foreach(Item item in items)
        {
            totalWeight += item.MyWeightInGrams;
        }

        return totalWeight;
    }

    // Maximale gewicht character is aantal slots * 10; 10 kilo is max een slot
    // scientist; 10 slots : 10 * 10 = 100 kilo
    // 15 kilo in backpack = 1 - ((15 - var) / 100) = 0,85 * speedModifier = newSpeed
    public float CalculateSpeedModifierInventory(int slotCount)
    {
        float maxWeightSlot = 10000.0f;
        float maxTotalWeight = slotCount * maxWeightSlot;
        float currentTotalWeight = CalculateCurrentTotalWeight();

        return 1 - (currentTotalWeight / maxTotalWeight);
    }

    /// <summary>
    /// Method calculates the number of used slots in the inventory
    /// </summary>
    /// <param name="bags"></param>
    /// <returns></returns>
    private int UsedSlots(List<Bag> bags)
    {
        int counter = 0;

        foreach(Bag bag in bags)
        {
            foreach (SlotScript slot in bag.MyBagScript.Slots)
            {
                if (slot.IsFull == true)
                {
                    counter++;
                }
            }
        }

        return counter;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class SlotScript : MonoBehaviour, IPointerClickHandler, IClickable
{
    /// <summary>
    /// A stack for all items on this slot
    /// </summary>
    private ObservableStack<Item> items = new ObservableStack<Item>();

    // A reference to the slot's icon
    [SerializeField] private Image icon;

    [SerializeField] private Text stackSize;

    // IsEmpty returns true if items.Count == 0, so we dont have any items in it
    /// <summary>
    /// Checks if the item is empty
    /// </summary>
    public bool IsEmpty
    {
        get
        {
            return items.Count == 0;
        }
    }

    public bool IsFull
    {
        get
        {
            if (IsEmpty)
            {
                return false;
            }
            else if (MyCount < MyItem.MyStackSize)
            {
                return false;
            }

            return true;
        }
    }

    public Item MyItem
    {
        get
        {
            if (!IsEmpty)
            {
                return items.Peek();
            }

            return null;
        }
    }

    public Image MyIcon
    {
        get
        {
            return icon;
        }
        set
        {
            icon = value;
        }
    }

    public int MyCount
    {
        get
        {
            return items.Count;
        }
    }

    public Text MyStackText
    {
        get
        {
            return stackSize;
        }
    }

    private void Awake()
    {
        items.OnPop += new UpdateStackEvent(UpdateSlot);
        //items.OnPop += new UpdateStackEvent(PrintPop);

        items.OnPush += new UpdateStackEvent(UpdateSlot);
        items.OnClear += new UpdateStackEvent(UpdateSlot);
    }

    private void PrintPop()
    {
        print("Test PrintPop method");
    }

    internal void Clear()
    {
        items.Clear();
    }



    /// <summary>
    /// Add an item(s) to the slot in the Inventory
    /// </summary>
    /// <param name="item"></param>
    public bool AddItem(Item item)
    {
        items.Push(item);
        icon.sprite = item.MyIcon;
        icon.color = Color.white;
        item.MySlot = this;

        return true;
    }

    public bool AddItems(ObservableStack<Item> newItems)
    {
        if (IsEmpty || newItems.Peek().GetType() == MyItem.GetType())
        {
            int count = newItems.Count;

            for (int i = 0; i < count; i++)
            {
                if (IsFull)
                {
                    return false;
                }

                AddItem(newItems.Pop());
            }

            return true;
        }

        return false;
    }


    public void RemoveItem(Item item)
    {
        if (items.Count > 0) // !IsEmpty
        {
            items.Pop();    // Remove the item
            //UIManager.MyInstance.UpdateStackSize(this); // place this on every method where you change the stacksize
        }
    }

    // Activate when we click on a slot

    /// <summary>
    /// When the slot is clicked
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left) // If the player left click on a slot to move an item
        {
            if (InventoryScript.MyInstance.FromSlot == null && !IsEmpty) // If i don't have anything in my hand to move
            {
                if (HandScript.MyInstance.MyMoveable != null)
                {
                    if (HandScript.MyInstance.MyMoveable is Bag)
                    {

                    }
                    else if (HandScript.MyInstance.MyMoveable is Armor)
                    {
                        if (MyItem is Armor && (MyItem as Armor).MyArmorType == (HandScript.MyInstance.MyMoveable as Armor).MyArmorType)
                        {
                            (MyItem as Armor).Equip();
                            HandScript.MyInstance.Drop();
                        }
                    }
                    else if (HandScript.MyInstance.MyMoveable is Weapon)
                    {
                        if (MyItem is Weapon && (MyItem as Weapon).MyArmorType == (HandScript.MyInstance.MyMoveable as Weapon).MyArmorType)
                        {
                            (MyItem as Weapon).Equip();
                            HandScript.MyInstance.Drop();
                        }
                    }
                }
                else
                {
                    HandScript.MyInstance.TakeMoveable(MyItem as IMoveable);
                    InventoryScript.MyInstance.FromSlot = this;
                }
            }
            else if (InventoryScript.MyInstance.FromSlot == null && IsEmpty)
            {
                if (HandScript.MyInstance.MyMoveable is Armor)
                {
                    Armor armor = (Armor)HandScript.MyInstance.MyMoveable;
                    AddItem(armor);
                    CharacterPanel.MyInstance.MySelectedButton.DequipArmor();
                    HandScript.MyInstance.Drop();
                }
                else if (HandScript.MyInstance.MyMoveable is Weapon)
                {
                    Weapon weapon = (Weapon)HandScript.MyInstance.MyMoveable;
                    AddItem(weapon);
                    CharacterPanel.MyInstance.MySelectedButton.DequipWeapon();
                    HandScript.MyInstance.Drop();
                }
            }
            else if (InventoryScript.MyInstance.FromSlot != null) // if i have something in my hand to move
            {
                if (PutItemBack() || SwapItems(InventoryScript.MyInstance.FromSlot) ||  AddItems(InventoryScript.MyInstance.FromSlot.items))
                {
                    HandScript.MyInstance.Drop();
                    InventoryScript.MyInstance.FromSlot = null;
                }
            }
        }

        // When right click on the button this results to true - when use something
        if (eventData.button == PointerEventData.InputButton.Right && HandScript.MyInstance.MyMoveable == null)
        {
            UseItem();
            if (items.Count <= 1)
                InventoryScript.MyInstance.itemCount--;
        }

        if (eventData.button == PointerEventData.InputButton.Middle)
        {
            DropItem(MyItem);
            
            if (items.Count <= 1)
                InventoryScript.MyInstance.itemCount--; // otherwise player can't loot item anymore

            MyItem.Remove();

            return;
        }
    }

    /// <summary>
    /// When item removes from inventory, the item will appear in the game
    /// </summary> 
    /// <param name="item"></param>
    private void DropItem(Item item)
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player"); // for the position of the player
        if (item.thisObject != null)
        {
            Instantiate(item.thisObject, new Vector3(player.transform.position.x, player.transform.position.y, player.transform.position.z), Quaternion.identity);
            return;
        }
    }

    /// <summary>
    /// Function when is executed when you right click on an item
    /// </summary>
    public void UseItem()
    {
        if (MyItem is IUseable)
        {
            (MyItem as IUseable).Use();
        }
        else if (MyItem is Armor)
        {
            (MyItem as Armor).Equip();

        }
        else if (MyItem is Weapon)
        {
            (MyItem as Weapon).Equip();
        }
    }

    public bool StackItem(Item item)
    {
        if (!IsEmpty && item.name == MyItem.name && items.Count < MyItem.MyStackSize)
        {
            items.Push(item);
            item.MySlot = this;
            InventoryScript.MyInstance.items.Add(item); // 01
            return true;
        }

        return false;
    }

    private bool PutItemBack()
    {
        if (InventoryScript.MyInstance.FromSlot == this)
        {
            InventoryScript.MyInstance.FromSlot.MyIcon.color = Color.white;
            return true;
        }

        return false;
    }

    private bool SwapItems(SlotScript from)
    {
        if (IsEmpty)
        {
            return false;
        }

        if (from.MyItem.GetType() != MyItem.GetType() || from.MyCount + MyCount > MyItem.MyStackSize)
        {
            // Copy all the items we need to swap for A
            ObservableStack<Item> tmpFrom = new ObservableStack<Item>(from.items);
            
            // Clear slot A
            from.items.Clear();
            // all items from slot b and copy them into A
            from.AddItems(items);

            // Clear B
            items.Clear();
            // Move the items from A Copy to B
            AddItems(tmpFrom);

            return true;
        }

        return false;
    }

    private void UpdateSlot()
    {
        UIManager.MyInstance.UpdateStackSize(this);
    }
}

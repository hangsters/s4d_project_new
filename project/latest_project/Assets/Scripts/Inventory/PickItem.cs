﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script must be placed on a lootable item
/// </summary>
public class PickItem : MonoBehaviour
{
    [SerializeField] private Item item;

    //[SerializeField] private GameObject tooltip; // can't because when gameobject deleted from inventory the tooltip serializefield isn't added

    private GameObject tooltip;
    private GameObject tooltipBackground;
    private InventoryScript inventoryScript;
    private Text tooltipText;
    private Image tooltipImage;
    private bool isObjectLootable;
    private bool toggle;
    private float currentTime;

    private void Awake()
    {
        tooltip = GameObject.Find("TooltipText");
        tooltipBackground = GameObject.Find("TooltipBackground");
    }

    // Start is called before the first frame update
    void Start()
    {
        inventoryScript = InventoryScript.MyInstance;

        // for Local and game purposes
        if (tooltip.GetComponent<Text>() != null)
            tooltipText = tooltip.GetComponent<Text>();
        else
            tooltipText = null;

        if (tooltipBackground.GetComponent<Image>() != null)
            tooltipImage = tooltipBackground.GetComponent<Image>();
        else
            tooltipImage = null;


        tooltipText.enabled = false;
        tooltipImage.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (item == null)
            return;

        if (!isObjectLootable)
            return;

        if (Input.GetKeyDown(KeyCode.E))
        {
            currentTime = Time.time;
            toggle = true;
        }

        if (Input.GetKey(KeyCode.E))
        {
            // When another item can't be placed in the inventory
            if (!InventoryScript.MyInstance.CanAddItem)
                return;

            if (Time.time - currentTime > item.LootTime && toggle)
            {
                TakeItem();
                AddItemToInventoryList(item);
            }
        }
    }

    /// <summary>
    /// Method when run when player enter the trigger
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer != 10) {
            return;
        }
        tooltipText.text = "Hold E " + item.LootTime + " sec in for loot " + item.name;
        tooltipText.enabled = true;
        tooltipImage.enabled = true;
        isObjectLootable = true;
    }

    /// <summary>
    /// Method when run when player exists the trigger
    /// </summary>
    /// <param name="other"></param>
    private void OnTriggerExit(Collider other)
    {
        tooltipText.text = " ";
        isObjectLootable = false;
    }

    /// <summary>
    /// Delete the gameObject from the game and resets the toggles
    /// </summary>
    private void TakeItem()
    {
        Destroy(gameObject);
        tooltipText.enabled = false;
        tooltipImage.enabled = false;
        toggle = false;
    }

    /// <summary>
    /// Adds the scriptable object to the list and display it in the Inventory UI
    /// </summary>
    /// <param name="item"></param>
    private void AddItemToInventoryList(Item item)
    {
        string itemType = item.GetType().ToString();

        if (itemType == AllItems.HealthPotion.ToString())
        {
            HealthPotion potion = (HealthPotion)Instantiate(item);
            inventoryScript.AddItem(potion);
        }
        else if (itemType == AllItems.FoodItem.ToString())
        {
            FoodItem food = (FoodItem)Instantiate(item) as FoodItem;
            inventoryScript.AddItem(food);
        }

        else if (itemType == AllItems.Bag.ToString())
        {
            Bag bag = (Bag)Instantiate(item);
            bag.Initialize(16);
            inventoryScript.AddItem(bag);
        }
        else if (itemType == AllItems.CraftingItem.ToString())
        {
            CraftingItem craftingItem = (CraftingItem)Instantiate(item) as CraftingItem;
            inventoryScript.AddItem(craftingItem);
        }
        else if (itemType == AllItems.Armor.ToString())
        {
            Armor armor = (Armor)Instantiate(item);
            inventoryScript.AddItem(armor);
        }
        else if (itemType == AllItems.Weapon.ToString())
        {
            Weapon weapon = (Weapon)Instantiate(item);
            inventoryScript.AddItem(weapon);
        }
    }
    
    /// <summary>
    /// Enum of all lootable item types in the game to pick up, name must match with ClassName
    /// </summary>
    enum AllItems
    {
        HealthPotion    = 0,
        Bag             = 1,
        CraftingItem    = 2,
        Armor           = 3,
        Weapon          = 4,
        FoodItem        = 5,
    }
}

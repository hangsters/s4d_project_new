﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CharacterButton : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private ArmorType armorType;
    [SerializeField] private Image icon;

    public Armor EquippedArmor;
    public Weapon EquippedWeapon;

    [HideInInspector] public int CurrentWeight;
    public float CurrentStrenght;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            // if user take armor from inventory to equipment
            if (HandScript.MyInstance.MyMoveable is Armor)
            {
                Armor tmp = (Armor)HandScript.MyInstance.MyMoveable;

                if (tmp.MyArmorType == this.armorType)
                {
                    EquipArmor(tmp);
                    return;
                }
            }
            // if user take weapon from inventory to equipment
            else if (HandScript.MyInstance.MyMoveable is Weapon)
            {
                Weapon tmp = (Weapon)HandScript.MyInstance.MyMoveable;

                if (tmp.MyArmorType == this.armorType)
                {
                    EquipWeapon(tmp);
                    return;
                }
            }
            // if uses takes armor from equipment to inventory
            else if (HandScript.MyInstance.MyMoveable == null)
            {
                if (EquippedArmor != null)
                {
                    HandScript.MyInstance.TakeMoveable(EquippedArmor);
                    icon.color = Color.grey;
                    CharacterPanel.MyInstance.MySelectedButton = this;
                }
                else if (EquippedWeapon != null)
                {
                    HandScript.MyInstance.TakeMoveable(EquippedWeapon);
                    icon.color = Color.grey;
                    CharacterPanel.MyInstance.MySelectedButton = this;
                }
            }
            
        }
    }

    public void EquipArmor(Armor armor)
    {
        armor.Remove();

        if (EquippedArmor != null)
        {
            if (EquippedArmor != armor)
            {
                armor.MySlot.AddItem(EquippedArmor);
            }
        }

        icon.enabled = true;
        icon.sprite = armor.MyIcon;
        icon.color = Color.white;
        this.EquippedArmor = armor;             // A reference to the equipped armor - for later use 
        
        if (HandScript.MyInstance.MyMoveable == (armor as IMoveable))
        {
            HandScript.MyInstance.Drop();
        }

        CurrentWeight = armor.MyWeightInGrams;
        CurrentStrenght = armor.MyStength;
        CharacterPanel.MyInstance.UpdateUI();
    }

    public void EquipWeapon(Weapon weapon)
    {
        weapon.Remove(); // removes the item from the inventory slot

        if (EquippedWeapon != null)
        {
            if (EquippedWeapon != weapon)
            {
                weapon.MySlot.AddItem(EquippedWeapon);
            }
        }

        icon.enabled = true;
        icon.sprite = weapon.MyIcon;
        icon.color = Color.white;
        this.EquippedWeapon = weapon;             // A reference to the equipped weapon - for later use 

        if (HandScript.MyInstance.MyMoveable == (weapon as IMoveable))
        {
            HandScript.MyInstance.Drop();
        }

        CurrentWeight = weapon.MyWeightInGrams;
    }

    public void DequipArmor()
    {
        icon.color = Color.white;
        icon.enabled = false;
        EquippedArmor = null;

        CurrentWeight = 0;
        CurrentStrenght = 0;
        CharacterPanel.MyInstance.UpdateUI();
    }

    public void DequipWeapon()
    {
        icon.color = Color.white;
        icon.enabled = false;
        EquippedWeapon = null;

        CurrentWeight = 0;
    }
}

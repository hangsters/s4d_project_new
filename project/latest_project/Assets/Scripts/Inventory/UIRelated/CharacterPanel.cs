﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class CharacterPanel : MonoBehaviour
{
    private static CharacterPanel instance;
    public int PlayerClassIndex;

    public static CharacterPanel MyInstance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<CharacterPanel>();
            }

            return instance;
        }
    }

    private CanvasGroup canvasGroup;

    /// <summary>
    /// fields for all the character slots in the game
    /// </summary>
    [SerializeField] public CharacterButton Head, Chest, Legs, Feet, MainHand, OffHand;
    
    /// <summary>
    /// Property of the selected character button
    /// </summary>
    public CharacterButton MySelectedButton { get; set; }

    /// <summary>
    /// Speed modifier of the EquipmentUI to read in the CharacterController script
    /// </summary>
    [HideInInspector] public float SpeedModifier;

    public Image ArmorFill;
    public Text ArmorText;

    // Start is called before the first frame update
    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
    }

    private void Start()
    {
        changeHeightPanel(PlayerClassIndex);
    }

    private void Update()
    {
        SpeedModifier = CalculateSpeedModifierEquipment();  // defines the speedModifier of the equipmentpanel
    }

    public void OpenClose()
    {
        if (canvasGroup.alpha <= 0)
        {
            canvasGroup.blocksRaycasts = true;
            canvasGroup.alpha = 1;
        }
        else
        {
            canvasGroup.blocksRaycasts = false;
            canvasGroup.alpha = 0;
        }
    }
    
    public void EquipArmor(Armor armor)
    {
        switch (armor.MyArmorType)
        {
            case ArmorType.Head:
                Head.EquipArmor(armor);
                break;
            case ArmorType.Chest:
                Chest.EquipArmor(armor);
                break;
            case ArmorType.Legs:
                Legs.EquipArmor(armor);
                break;
            case ArmorType.Feet:
                Feet.EquipArmor(armor);
                break;
        }
    }

    public void DequipArmor(Armor armor)
    {
        switch (armor.MyArmorType)
        {
            case ArmorType.Head:
                Head.DequipArmor();
                break;
            case ArmorType.Chest:
                Chest.DequipArmor();
                break;
            case ArmorType.Legs:
                Legs.DequipArmor();
                break;
            case ArmorType.Feet:
                Feet.DequipArmor();
                break;
        }
    }

    public void EquipWeapon(Weapon weapon)
    {
        switch (weapon.MyArmorType)
        {
            case ArmorType.MainWeapon:
                MainHand.EquipWeapon(weapon);
                break;
            case ArmorType.OffWeapon:
                OffHand.EquipWeapon(weapon);
                break;
        }
    }

    /// <summary>
    /// With the use of try/catch a error won't occur
    /// </summary>
    /// <returns></returns>
    public float CalculateCurrentArmor()
    {
        //return Head.CurrentStrenght + Legs.CurrentStrenght + Chest.CurrentStrenght + Feet.CurrentStrenght;
        
        float totalArmor = 0;
        try
        {
            totalArmor += Head.EquippedArmor.MyStength;
        }
        catch
        { }
        try
        {
            totalArmor += Chest.EquippedArmor.MyStength;
        }
        catch
        { }
        try
        {
            totalArmor += Legs.EquippedArmor.MyStength;
        }
        catch
        { }
        try
        {
            totalArmor += Feet.EquippedArmor.MyStength;
        }
        catch
        { }

        return totalArmor;
    }

    /// <summary>
    /// Gives a list of the equipped items of the equipment UI
    /// </summary>
    /// <returns></returns>
    public List<int> CountEquippedArmor()
    {
        List<int> equippedArmor = new List<int>();
        if (Head.EquippedArmor != null)
            equippedArmor.Add(0);
        if (Chest.EquippedArmor != null)
            equippedArmor.Add(1);
        if (Legs.EquippedArmor != null)
            equippedArmor.Add(2);
        if (Feet.EquippedArmor != null)
            equippedArmor.Add(3);

        return equippedArmor;
    }

    /// <summary>
    /// Update the equipment (armor) UI bar above the screen
    /// </summary>
    public void UpdateUI()
    {
        int maxArmor = 200;
        ArmorFill.fillAmount = CalculateCurrentArmor() / maxArmor;
        ArmorText.text = ((int) CalculateCurrentArmor()).ToString() + " (200)";
    }

    /// <summary>
    /// Method to calculate the current total weight of the equipment
    /// </summary>
    /// <returns></returns>
    private int CalculateCurrentTotalWeight()
    {
        return Head.CurrentWeight + Legs.CurrentWeight + Chest.CurrentWeight + Feet.CurrentWeight + MainHand.CurrentWeight + OffHand.CurrentWeight;
    }

    /// <summary>
    /// Calculates the speedmodifier of the equipmentUI - float between 0.0 and 1.0
    /// </summary>
    /// <returns></returns>
    public float CalculateSpeedModifierEquipment()
    {
        float maxTotalWeight = 6 * 10000;
        int currentTotalWeight = CalculateCurrentTotalWeight();
        
        return 1 - (currentTotalWeight / maxTotalWeight); // getal tussen de 0.1 en 1.0 - 1.0 is het hoogst, rent het snelst
    }
    
    public void changeHeightPanel(int playerClassIndex)
    {
        int[] positions = { 530, 375, 430};
        if (playerClassIndex == 0)
            this.transform.position = new Vector3(transform.position.x, positions[playerClassIndex], transform.position.z);
        else if (playerClassIndex == 1)
            this.transform.position = new Vector3(transform.position.x, positions[playerClassIndex], transform.position.z);
        else if (playerClassIndex == 2)
            this.transform.position = new Vector3(transform.position.x, positions[playerClassIndex], transform.position.z);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class HandScript : MonoBehaviour
{
    /// <summary>
    /// Singleton instance of the handscript
    /// </summary>
    private static HandScript instance;

    public static HandScript MyInstance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<HandScript>();
            }

            return instance;
        }
    }

    /// <summary>
    /// The current moveable
    /// </summary>
    public IMoveable MyMoveable { get; set; }

    /// <summary>
    /// The icon of the item, that we acre moving atm 
    /// </summary>
    private Image icon;

    [SerializeField] private Vector3 offset;

    private void Start()
    {
        // Creates a reference to the image on the hand
        icon = GetComponent<Image>();
    }

    private void Update()
    {
        // Makes sure that the cion follows the hand
        icon.transform.position = Input.mousePosition + offset;

        //if (Input.GetMouseButtonDown(0) && EventSystem.current.IsPointerOverGameObject())
        //{
        //    DeleteItem();
        //}
    }

    /// <summary>
    /// Take a moveable in the hand, so that we can move it around
    /// </summary>
    /// <param name="moveable"></param>
    public void TakeMoveable(IMoveable moveable)
    {
        this.MyMoveable = moveable;
        icon.sprite = moveable.MyIcon;
        icon.color = Color.white;
    }

    public IMoveable Put()
    {
        IMoveable tmp = MyMoveable;
        MyMoveable = null;
        icon.color = new Color(0, 0, 0, 0);
        return tmp;
    }

    public void Drop()
    {
        MyMoveable = null;
        icon.color = new Color(0, 0, 0, 0);
        InventoryScript.MyInstance.FromSlot = null;
    }

    public void DeleteItem()
    {
        if (MyMoveable is Item && InventoryScript.MyInstance.FromSlot != null)
        {
            (MyMoveable as Item).MySlot.Clear();
        }

        Drop();

        InventoryScript.MyInstance.FromSlot = null;
    }
}

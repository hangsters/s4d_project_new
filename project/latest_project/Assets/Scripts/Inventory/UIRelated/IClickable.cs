﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// IClickable is used whenever we click something with the mouse
public interface IClickable
{
    Image MyIcon
    {
        get;
        set;
    }

    // How many items are on the slot
    int MyCount
    {
        get;
    }

    Text MyStackText
    {
        get; 
    }
}

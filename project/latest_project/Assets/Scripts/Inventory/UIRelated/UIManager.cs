﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour

{
    public bool isScientist = false;
    public GameObject craftingBench;

    private static UIManager instance;

    // Check if only one UIManager have this script added to
    // singleton
    public static UIManager MyInstance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<UIManager>();
            }

            return instance;
        }
    }

    // ----------------------------------------------------------

    // Update is called once per frame
    void Update()
    {
        // Open and closes the inventoryUI
        if (Input.GetKeyDown(KeyCode.I))
        {
            InventoryScript.MyInstance.OpenClose();
        }

        // Open and closes the CharacterPanelUI
        if (Input.GetKeyDown(KeyCode.C))
        {
            CharacterPanel.MyInstance.OpenClose();
        }

        if (isScientist)
        {
            if (Input.GetKeyDown(KeyCode.L))
            {
                craftingBench.SetActive(!craftingBench.activeInHierarchy);
            }
        }
    }

    /// <summary>
    /// Updates the stacksize on a clickable slot
    /// </summary>
    /// <param name="clickable"></param>
    public void UpdateStackSize(IClickable clickable)
    {
        if (clickable.MyCount > 1)
        {
            clickable.MyStackText.text = clickable.MyCount.ToString();
            clickable.MyStackText.color = Color.white;
            clickable.MyIcon.color = Color.white;
        }
        else
        {
            clickable.MyStackText.color = new Color(0, 0, 0, 0);
        }


        if (clickable.MyCount == 0) // if the slot is empty, then we need to hide the icon
        {
            clickable.MyIcon.color = new Color(0, 0, 0, 0); // Hide icon when it's empty
            clickable.MyStackText.color = new Color(0, 0, 0, 0);
        }
    }
}

﻿using UnityEngine;

[CreateAssetMenu]
public class LootableObject : ScriptableObject
{
    public string Name;
    public string Description;
    public Sprite ItemIcon;
    public int HealthIncrease;
    public int WeightInGrams;
    public int LootTime;

    // for different items, we want different things to happen when we try and use them. 
    // By marking this as virtual we can simple derive different objects from the LootableObject
    public virtual void Use()
    {
        // Use the item
        // Something might happen

        Debug.Log("Using " + Name);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Armor", menuName = "Items/Armor", order = 2)]
public class Armor : Item
{
    [SerializeField] private float strength;
    [SerializeField] private int stamina;
    [SerializeField] private ArmorType armorType;

    internal ArmorType MyArmorType
    {
        get
        {
            return armorType;
        }
    }

    public void Equip()
    {
        CharacterPanel.MyInstance.EquipArmor(this);
    }

    public float MyStength
    {
        get
        {
            return strength;
        }
        set
        {
            strength = value;
        }
    }

}

public enum ArmorType { Head, Chest, Legs, Feet, MainWeapon, OffWeapon }

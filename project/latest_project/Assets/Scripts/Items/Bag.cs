﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Bag", menuName = "Items/Bag", order = 1)]
public class Bag : Item, IUseable
{
    private int slots;

    /// <summary>
    /// A Reference to a bag prefab, so that we can instantiate a bag in the game
    /// </summary>
    [SerializeField] protected GameObject bagPrefab;

    /// <summary>
    /// A reference to the bagScript, that this bag belongs to
    /// </summary>
    public BagScript MyBagScript;

    /// <summary>
    /// Property for getting the slots
    /// </summary>
    public int Slots
    {
        get
        {
            return slots;
        }
    }

    // Function to create the slots
    public void Initialize(int slots)
    {
        this.slots = slots;
    }

    public void Use()
    {
        // can add bag if the player has less than 3 bags
        // BagScript is already on bag prefab because it sits in the prefab of the bag
        if (InventoryScript.MyInstance.CanAddBag)
        {
            Remove();

            MyBagScript = Instantiate(bagPrefab, InventoryScript.MyInstance.transform).GetComponent<BagScript>();   // Createh a bagPrefab and Get the BagScript component of the bagPrefab
            MyBagScript.AddSlots(slots);                                                

            InventoryScript.MyInstance.AddBag(this);
        }
    }
}

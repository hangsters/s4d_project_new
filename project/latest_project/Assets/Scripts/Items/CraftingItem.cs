﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Opmerking
// Player moet excact Player heten

[CreateAssetMenu(fileName = "CraftingItem", menuName = "Items/CraftingItem", order = 1)]
public class CraftingItem : Item
{
    public string Description;
}

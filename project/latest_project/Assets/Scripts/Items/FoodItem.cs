﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Opmerking
// Player moet excact Player heten

[CreateAssetMenu(fileName = "FoodItem", menuName = "Items/FoodItem", order = 1)]
public class FoodItem : Item, IUseable
{
    public Item emptyContainer;

    [SerializeField] public int healing;

    private GameObject player;
    private PlayerHealthManagement playerHealthManagement;

    public void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealthManagement = player.GetComponent<PlayerHealthManagement>();
    }

    public void Use()
    {
        if (playerHealthManagement.health < playerHealthManagement.maxHealth)
        {
            Remove();
            playerHealthManagement.HealPlayer(healing);

            string itemType = emptyContainer.GetType().ToString();
            if (itemType == AllItems.CraftingItem.ToString())
            {
                CraftingItem output = (CraftingItem)Instantiate(emptyContainer);
                InventoryScript.MyInstance.AddItem(output);
            }
        }
    }
enum AllItems
    {
        HealthPotion    = 0,
        Bag             = 1,
        CraftingItem    = 2,
        Armor           = 3,
        Weapon          = 4,
        FoodItem        = 5,
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Opmerking
// Player moet excact Player heten

[CreateAssetMenu(fileName = "HealthPotion", menuName = "Items/HealthPotion", order = 1)]
public class HealthPotion : Item, IUseable
{
    [SerializeField] public int healing;

    private GameObject player;
    private PlayerHealthManagement playerHealthManagement;

    public void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealthManagement = player.GetComponent<PlayerHealthManagement>();
        //playerHealthManagement = FindObjectOfType<PlayerHealthManagement>(); // heeft als nadeel dat wanneer 2 van deze scripts zijn ingeladen, hij 1 random script kiest.
    }

    public void Use()
    {
        if (playerHealthManagement.health < playerHealthManagement.maxHealth)
        {
            Remove();
            playerHealthManagement.HealPlayer(healing);
        }
    }
}

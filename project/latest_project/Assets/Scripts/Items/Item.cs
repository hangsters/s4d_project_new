﻿using UnityEngine;

// Superclass for all the lootable items
// A scriptable object is an object that exist in Unity without a GameObject
// Must only contain fields for purpose for all lootalble items

//enum itemType { Helmet, Chest, Pants, Boots, Weapon, None }

/// <summary>
/// Superclass for all items
/// </summary>
public abstract class Item : ScriptableObject, IMoveable
{
    /// <summary>
    /// Icon used when moving and placing the items
    /// </summary>
    [SerializeField] private Sprite icon;               // icon that will be display on the inventory UI
    public GameObject thisObject;
    public string Name;

    /// <summary>
    /// The size of the stack, less than 2 is not stackable
    /// </summary>
    [SerializeField] private int stackSize;             // if is 0 or 1 it's not stackable, if stackSize > 1 it's stackable.
    [SerializeField] private int weightInGrams;         // the weight of the item. Every slot is 1kg, if item weigth 200 gram, stackSize = 5;
    [SerializeField] public int LootTime;

    // An item only exist in the game if it's placed in the inventory
    // if it's outside the inventory ar outside loottable it doesnt exists

    /// <summary>
    /// A reference to the slot that this item is setting on
    /// </summary>
    private SlotScript slot;

    /// <summary>
    /// Property for accessing the icon
    /// </summary>
    public Sprite MyIcon
    {
        get
        {
            return icon;
        }
    }

    /// <summary>
    /// Property for accessing the stacksize
    /// </summary>
    public int MyStackSize
    {
        get
        {
            return stackSize;
        }
    }

    /// <summary>
    /// Property for accessing the slotscript
    /// </summary>
    public SlotScript MySlot
    {
        get
        {
            return slot;
        }

        set
        {
            slot = value;
        }
    }

    public int MyWeightInGrams
    {
        get
        {
            return weightInGrams;
        }
    }

    public void Remove()
    {
        if (MySlot != null)
        {
            InventoryScript.MyInstance.items.Remove(this); // 01
            MySlot.RemoveItem(this);
        }
    }
}

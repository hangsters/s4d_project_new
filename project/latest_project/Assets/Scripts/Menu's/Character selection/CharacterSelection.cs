﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSelection : MonoBehaviour
{
    public List<PlayerClass> classes;
    public Text charText;

    private int characterIndex = 0;
    private float xTransform = 0;
    private void Start()
    {
        changeCharacter();
    }

    private void Update()
    {
        transform.position = new Vector3(Mathf.Lerp(transform.position.x, xTransform, Time.deltaTime * 4), 0, 0);
    }
    public void changeCharacter()
    {
        charText.text = classes[characterIndex].Name;
        PlayerPrefs.SetInt("PlayerClass", characterIndex);
    }
    public void characterIndexPlus()
    {
        if(characterIndex < classes.Count-1)
        {
            characterIndex++;
            xTransform -= 10;
        }
        else
        {
            characterIndex = 0;
            xTransform = 0;
        }
        changeCharacter();
    }

    public void characterIndexMinus()
    {
        if (characterIndex > 0)
        {
            characterIndex--;
            xTransform += 10;
        }
        else
        {
            characterIndex = classes.Count - 1;
            xTransform = -20;
        }
        changeCharacter();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CabinetScript : Interactable
{
    public Transform spawnPoint;
    public LootTable thisLoot;

    protected override void decideOnInteraction(Collider other)
    {

        if (!opened)
        {
            if (other.tag == "Player")
            {
                goOnWithInteraction();
            }
        }
    }

    protected override string hintText()
    {
        return "search " + Name;
    }

    protected override void Pressed()
    {
        var ap = GetComponent<AudioSource>();
        base.Pressed();
        generateLoot();
        ap.Play();
    }

    private void generateLoot()
    {
        if (thisLoot != null)
        {
            Item current = thisLoot.LootItem();
            if(current != null)
            {
                Instantiate(current.thisObject, spawnPoint);
            }
        }
    }

}
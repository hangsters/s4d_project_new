﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChestScript : Interactable
{
    public Transform spawnPoint;
    public LootTable thisLoot;
    protected override void decideOnInteraction(Collider other)
    {
        //base.decideOnInteraction(other);

        if (!opened)
        {
            if (other.tag == "Player")
            {
                goOnWithInteraction();
            }
        }
    }

    protected override string hintText()
    {
            return "open " + Name;
    }

    private void generateLoot()
    {
        if (thisLoot != null)
        {
            Item current = thisLoot.LootItem();
            if (current != null)
            {
                Instantiate(current.thisObject, spawnPoint);
            }
        }
    }

    protected override void Pressed()
    {
        var ap = GetComponent<AudioSource>();
        base.Pressed();
        generateLoot();
        ap.Play();
    }

}

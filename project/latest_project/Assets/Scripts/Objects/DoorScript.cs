﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DoorScript : Interactable
{
    public AudioClip open;
    public AudioClip close;

    private AudioSource doorSoundEffect;

    protected override string hintText()
    {
        if (opened)
            return "close " + Name;
        else
            return "open " + Name;
    }

    protected override void Pressed()
    {   
        base.Pressed();
        doorSoundEffect = GetComponent<AudioSource>();
        if (opened)
        {
            doorSoundEffect.PlayOneShot(open);
        }
        else
        {
            doorSoundEffect.PlayOneShot(close);
        }
    }



}





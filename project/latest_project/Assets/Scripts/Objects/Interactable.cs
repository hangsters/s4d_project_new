﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interactable : MonoBehaviour
{
    private  Animator anim;
    protected bool opened = false;
    private bool searchForKeyDown = false;

    public string Name;

    public bool InteractableState;
    private GameObject UiTextObject;
    private Text UiTextObject2;
    private GameObject UiTextBack;

    private void Awake()
    {
        UiTextObject = GameObject.FindGameObjectWithTag("UI Text");
        UiTextObject2 = UiTextObject.GetComponent<Text>();
        UiTextBack = GameObject.FindGameObjectWithTag("UI Background");
    }

    private void Start()
    {
        if (UiTextObject.activeInHierarchy)
        {
            UiTextObject.SetActive(false);
            UiTextBack.SetActive(false);
        }        
    }
    // Update is called once per frame
    public void Update()
    {
       if (searchForKeyDown) 
       {
            if (InteractableState)
            {
                UiTextObject2.text = "Press E to " + hintText();
            }
            if (Input.GetKeyDown(KeyCode.E))
            {
                Pressed();
                
                //Delete if you dont want Text in the Console saying that You Press F.
                
            }
       }
    }

    public void OnTriggerEnter(Collider other)
    {
        decideOnInteraction(other);
    }

    protected virtual void decideOnInteraction(Collider other)
    {
        if (other.tag == "Player")
        {
            goOnWithInteraction();
        }
    }

    protected virtual string hintText()
    {
        return "?????????";
    }

    public void goOnWithInteraction()
    {
        searchForKeyDown = true;
        UiTextObject2.text = "Press E to " + hintText();
        UiTextObject.SetActive(true);
        UiTextBack.SetActive(true);
    }
    public void OnTriggerExit(Collider other)
    {
        searchForKeyDown = false;
        UiTextObject.SetActive(false);
        UiTextBack.SetActive(false);
        print(" ");
    }
    protected virtual void Pressed()
    {
        //This line will get the Animator from  Parent of the door that was hit by the raycast.
        anim = this.transform.GetComponentInParent<Animator>();
        
        
        //This will set the bool the opposite of what it is.
        opened = !opened;
        if (!InteractableState)
        {
            UiTextObject.SetActive(false);
            UiTextBack.SetActive(false);
        }
        

        //This line will set the bool true so it will play the animation.
        anim.SetBool("Opened", opened);
    }
}

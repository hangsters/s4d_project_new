﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkbenchScript : MonoBehaviour
{
    public GameObject craftingBench;
    public FireWeapon fireScript;
    private bool interact = false;

    private void Update()
    {
        interact = Input.GetKey(KeyCode.E);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            craftingBench.SetActive(!craftingBench.activeInHierarchy);
            fireScript.enabled = false;

            if (!InventoryScript.MyInstance.Bags[0].MyBagScript.IsOpen)
                InventoryScript.MyInstance.OpenClose();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            craftingBench.SetActive(!craftingBench.activeInHierarchy);
            InventoryScript.MyInstance.OpenClose();
            fireScript.enabled = true;
        }
    }
    
}

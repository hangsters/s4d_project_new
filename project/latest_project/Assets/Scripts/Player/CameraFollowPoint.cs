﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowPoint : MonoBehaviour
{
    public Camera MyCamera;
    public GameObject PlayerObject;

    public InputHandler inputHandler;
    private Vector3 target;
    private Vector3 followPosition;
    
    private void Update()
    {
        SetFollowPosition();
    }

    private void SetFollowPosition()
    {
        Ray ray = MyCamera.ScreenPointToRay(inputHandler.mouseInput);
        if (Physics.Raycast(ray, out RaycastHit hitInfo, maxDistance: 300f))
        {
            target = hitInfo.point;
            target.y = 0f;
        }

        followPosition = (target + PlayerObject.transform.position) / 2;
        followPosition = (followPosition + PlayerObject.transform.position) / 2;
        transform.position = followPosition;
    }
}

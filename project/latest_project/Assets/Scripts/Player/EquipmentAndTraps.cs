﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentAndTraps : MonoBehaviour
{
    private float maxVel = 15;
    private float minVel = 3;
    private float currentVel;
    public LineRenderer trajectory;

    public GameObject projectile;
    public float chargeUpTime = 1f;
    public Transform HandTransform;
    public int resolution = 50;
    private float angle = 45;
    float radianAngle;

    public void Start()
    {
        currentVel = minVel;
    }    

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            Charge();
            Debug.Log(currentVel);
        }
        if (Input.GetKeyUp(KeyCode.Q))
        {
            Throw(currentVel);
        }
    }

    private void Charge()
    {
        trajectory.enabled = true;
        currentVel = Mathf.Lerp(currentVel, maxVel, Time.deltaTime / chargeUpTime);
        RenderArc(currentVel);
    }

    private void RenderArc(float velocity)
    {
        trajectory.SetVertexCount(resolution + 1);
        trajectory.SetPositions(CalculateArcArray(velocity));
    }

    Vector3[] CalculateArcArray(float velocity)
    {
        Vector3[] arcArray = new Vector3[resolution + 1];

        radianAngle = Mathf.Deg2Rad * angle;
        float maxDistance = (velocity * velocity * Mathf.Sin(2 * radianAngle)) / 9.81f;
        for(int i = 0; i <= resolution; i++)
        {
            float t = (float)i / (float)resolution;
            arcArray[i] = CalculateArcPoint(t, maxDistance, velocity);
        }
        return arcArray;
    }

    Vector3 CalculateArcPoint(float t, float maxDistance, float velocity)
    {
        float x = t * maxDistance;
        float y = x * Mathf.Tan(radianAngle) - ((9.81f * x * x) / (2 * velocity * velocity * Mathf.Cos(radianAngle) * Mathf.Cos(radianAngle)));
        return new Vector3(0, y, x);
    }

    private void Throw(float velocity)
    {
        GameObject currentProjectile = Instantiate(projectile, HandTransform.position, Quaternion.identity);
        Rigidbody rb = currentProjectile.GetComponent<Rigidbody>();
        rb.velocity = transform.TransformDirection(0, velocity * Mathf.Sqrt(2) / 2, velocity * Mathf.Sqrt(2) / 2);
        currentVel = minVel;
        trajectory.enabled = false;
    }
}

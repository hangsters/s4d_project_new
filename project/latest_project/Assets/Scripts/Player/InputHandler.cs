﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    [HideInInspector]
    public Vector3 moveInput;
    [HideInInspector]
    public Vector3 mouseInput;

    [HideInInspector]
    public bool crouchInput = false;
    [HideInInspector]
    public bool sprintInput = false;

    [HideInInspector]
    public bool firstGun = false;
    [HideInInspector]
    public bool secondGun = false;
    [HideInInspector]
    public bool thirdGun = false;


    private void Update()
    {
        moveInput = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        mouseInput = Input.mousePosition;

        firstGun = Input.GetKeyDown(KeyCode.Alpha1);
        secondGun = Input.GetKeyDown(KeyCode.Alpha2);
        thirdGun = Input.GetKeyDown(KeyCode.Alpha3);

        sprintInput = Input.GetKey(KeyCode.LeftShift);

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            crouchInput = !crouchInput;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : MonoBehaviour
{
    private CharacterController controller;
    private int playerClassIndex = 0;
    private float currentStamina;
    private float startStamina = 5;
    private StaminaUI staminaUI;
    private float cooldownTime = 3.0f;
    private float gravity = 9.8f;
    private float vSpeed = 0;
    private bool canSprint = true;
    private bool replenishing = false;
    private bool isMoving = false;
    private PlayerHealthManagement playerHealth;
    private FireWeapon playerWeapon;
    private float speedModifier;

    public List<PlayerClass> PlayerClasses;
    public Camera cam;
    public InputHandler inputHandler;
    public float SprintSpeed;
    public float WalkSpeed;
    public float CrouchSpeed;
    public Animator StaminaAnim;
    public LayerMask AudioCast;

    public int usableClassIndex;

    private void Awake()
    {
        playerWeapon = GetComponent<FireWeapon>();
        playerHealth = GetComponent<PlayerHealthManagement>();
        controller = GetComponent<CharacterController>();
        staminaUI = GetComponent<StaminaUI>();        
        playerClassIndex = PlayerPrefs.GetInt("PlayerClass");
        playerHealth.maxHealth = playerHealth.maxHealth * PlayerClasses[playerClassIndex].HealthModifier;
        InventoryScript.MyInstance.SlotSize = PlayerClasses[playerClassIndex].Slots;
        CharacterPanel.MyInstance.PlayerClassIndex = playerClassIndex;
    }

    private void Start()
    {
        SprintSpeed = SprintSpeed * PlayerClasses[playerClassIndex].SpeedModifier;
        WalkSpeed = WalkSpeed * PlayerClasses[playerClassIndex].SpeedModifier;
        CrouchSpeed = CrouchSpeed * PlayerClasses[playerClassIndex].SpeedModifier;
        startStamina = startStamina * PlayerClasses[playerClassIndex].StaminaModifier;
        currentStamina = startStamina;

        if (PlayerClasses[playerClassIndex].Name == "Scientist")
        {
            UIManager.MyInstance.isScientist = true;
        }

    }

    // Update is called once per frame
    void Update()
    {
        castFootstepSphere();

        speedModifier = CalculateSpeedModifier(); // Gets the speedmodifier based on the inventory and equipment

        if (inputHandler.crouchInput || playerWeapon.isAiming)
        {
            Move(CrouchSpeed);
            if (!replenishing)
            {
                StartCoroutine(ReplenishStamina());
            }
        }
        else if(inputHandler.sprintInput)
        {
            if (isMoving)
            {
                if (currentStamina > 0)
                {
                    if (canSprint)
                    {
                        if (replenishing)
                        {
                            StopAllCoroutines();
                            replenishing = false;
                        }
                        StaminaAnim.SetBool("isSprinting", true);
                        Move(SprintSpeed);
                        //playerWeapon.setFOV(playerWeapon.Weapons[playerWeapon.currentWeaponIndex].BulletSpreadSprint);
                        currentStamina -= Time.deltaTime;
                    }
                    else
                    {
                        Move(WalkSpeed);
                    }
                }
                else
                {
                    canSprint = false;
                    Move(WalkSpeed);
                    currentStamina = 0;
                    if (!replenishing)
                    {
                        StartCoroutine(ReplenishStamina());
                    }
                }
            }                      
        }
        else
        {
            Move(WalkSpeed);
            //playerWeapon.setFOV(playerWeapon.Weapons[playerWeapon.currentWeaponIndex].BulletSpreadInDeg);
            if (!replenishing)
            {
                StartCoroutine(ReplenishStamina());
            }
        }
        rotateTowardMouse();
        staminaUI.updateStaminaUI(currentStamina, startStamina);
    }

    private float CalculateSpeedModifier()
    {
        int slotSizeInventory = (PlayerClasses[playerClassIndex].Slots == 0) ? 15 : PlayerClasses[playerClassIndex].Slots; // Gets the count of slots
        float speedModifierBasedOnInventory;
        float speedModifierBasedOnEquipment;

        try // check if there's an inventoryscript on the player
        {
            speedModifierBasedOnInventory = InventoryScript.MyInstance.SpeedModifier; 
        }
        catch // default speedModifierInventory
        {
            speedModifierBasedOnInventory = 1.0f; 
        }
        try // check if there's a characterpanel (equipmentUI) on the player
        {
            speedModifierBasedOnEquipment = CharacterPanel.MyInstance.SpeedModifier; 
        }
        catch // default speedModifierEquipment
        {
            speedModifierBasedOnEquipment = 1.0f; 
        }

        float totalSlots = slotSizeInventory + 6.0f;

        float newSpeedModifierInventory = (slotSizeInventory / totalSlots) * speedModifierBasedOnInventory;
        float newSpeedModfierEquipment = (6 / totalSlots) * speedModifierBasedOnEquipment;

        //print("speed modifier: " + (newSpeedModifierInventory + newSpeedModfierEquipment));
        return (newSpeedModifierInventory + newSpeedModfierEquipment) + 0.3f;
        //return 1.0f;
    }

    private void Move(float speed)
    {
        speed = speed * speedModifier; // speedModifier is the reference in the Update method to the CalculateSpeedModifier() method
        if (controller.isGrounded)
        {
            vSpeed = -controller.stepOffset / Time.deltaTime;
        }
        else
        {
            vSpeed -= gravity * Time.deltaTime;
        }
        vSpeed -= gravity * Time.deltaTime;

        if (inputHandler.moveInput == Vector3.zero)
        {
            isMoving = false;
        }
        else
        {
            isMoving = true;
            Vector3 velocity = inputHandler.moveInput * speed;
            velocity.y = vSpeed;
            velocity = Vector3.ClampMagnitude(velocity, speed);
            controller.Move(velocity * Time.deltaTime);
        }        
    }

    private void rotateTowardMouse()
    {
        Ray ray = cam.ScreenPointToRay(inputHandler.mouseInput);
        if(Physics.Raycast(ray,out RaycastHit hitInfo, maxDistance: 300f))
        {
            var target = hitInfo.point;
            target.y = transform.position.y;
            transform.LookAt(target);
        }
    }

    private void castFootstepSphere()
    {        
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, WalkSpeed, AudioCast);
        foreach(Collider col in hitColliders)
        {
            if(col.gameObject.tag == "AI")
            {
                AIBehaviour ai = col.gameObject.GetComponent<AIBehaviour>();
                if(ai != null)
                {
                    ai.playerInHearingRange = true;
                }                
            }            
        }
    }

    private IEnumerator ReplenishStamina()
    {
        replenishing = true;
        while(Mathf.Abs(startStamina - currentStamina) > 0.01f)
        {
            currentStamina += Time.deltaTime;
            staminaUI.updateStaminaUI(currentStamina, startStamina);
            yield return new WaitForSeconds(Time.deltaTime);
        }
        if (!inputHandler.sprintInput)
        {
            StaminaAnim.SetBool("isSprinting", false);
        }        
        replenishing = false;
        canSprint = true;
        currentStamina = startStamina;        
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, CrouchSpeed);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, WalkSpeed);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, SprintSpeed);
    }
}

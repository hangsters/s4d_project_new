﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Opmerkingen:

public class PlayerHealthManagement : MonoBehaviour
{
    // Singleton
    private static PlayerHealthManagement instance;
    public static PlayerHealthManagement MyInstance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType<PlayerHealthManagement>();

            return instance;
        }
    }
    // Singleton ends
    
    // declaring fields
    [HideInInspector] public float maxHealth = 100.0f;
    [SerializeField] public float health;
    public GameObject damageText;
    public Image healthFill;
    public Text healthText;

    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        updateUI();
    }

    // Update is called once per frame
    void Update()
    {
        // Destory gameObject if health <= 0, these lines of code can also in the TakeDamage() function
        if (this.health <= 0)
            Die();
    }

    /// <summary>
    /// TakeDamage will be call when the player gets a hit
    /// </summary>
    /// <param name="damage"></param>
    public void TakeDamage(float damage) /*, float damageLeft = 0*/
    {
        if (CanArmorTakeHit(damage))
            return;

        this.health -= Mathf.RoundToInt(damage);
        showDamage(Mathf.RoundToInt(damage));
        updateUI();
    }

    /// <summary>
    /// Check if the armor can take the damage of the hit, false -> player health gets decrease
    /// </summary>
    /// <param name="damage"></param>
    /// <returns></returns>
    private bool CanArmorTakeHit(float damage) 
    {
        try
        {
            var countArmor = CharacterPanel.MyInstance.CountEquippedArmor();
            if (countArmor.Count == 0)
            {
                return false;
            }
            else if (countArmor.Count >= 1)
            {
                float damageItem = damage / countArmor.Count;
                print(damageItem);
                List<Armor> equipmentItems = new List<Armor>();
                equipmentItems.Add(CharacterPanel.MyInstance.Head.EquippedArmor);
                equipmentItems.Add(CharacterPanel.MyInstance.Chest.EquippedArmor);
                equipmentItems.Add(CharacterPanel.MyInstance.Legs.EquippedArmor);
                equipmentItems.Add(CharacterPanel.MyInstance.Feet.EquippedArmor);

                foreach (int i in countArmor)
                {
                    CheckArmor(equipmentItems[i], damageItem);
                }
            }

            return true;
        }
        catch
        {
            return false;
        }
    }

    /// <summary>
    /// Method that's checked if single piece of armor can take the damage of the hit
    /// </summary>
    /// <param name="armor"></param>
    /// <param name="damage"></param>
    private void CheckArmor(Armor armor, float damage)
    {
        armor.MyStength -= damage;
        CharacterPanel.MyInstance.UpdateUI();

        if (Mathf.Round(armor.MyStength * 100f) / 100f <= 0)
            CharacterPanel.MyInstance.DequipArmor(armor);
    }


    /// <summary>
    /// Update the health bar when playe is hit
    /// </summary>
    private void updateUI()
    {
        healthFill.fillAmount = health / maxHealth;
        healthText.text = health.ToString();
    }

    /// <summary>
    /// Show the damage above the player when player is hit by enemy
    /// </summary>
    /// <param name="damage"></param>
    private void showDamage(int damage)
    {
        GameObject DamagePopup = Instantiate(damageText, transform.position + new Vector3(Random.Range(-1,1), 2, 0.5f), Quaternion.Euler(90,0,0));
        damagePopup damagetext = DamagePopup.GetComponent<damagePopup>();
        damagetext.setup(damage);
    }

    public void bleedDamage(float bleedTime, float bleedInterval, int damagePerBleed)
    {
        if(bleedTime > 0)
        {
            TakeDamage(damagePerBleed);
            StartCoroutine(bleedWait(bleedTime, bleedInterval, damagePerBleed));
        }        
    }

    private IEnumerator bleedWait(float bleedTime, float bleedInterval, int damagePerBleed)
    {
        yield return new WaitForSeconds(bleedInterval);
        bleedTime -= bleedInterval;
        bleedDamage(bleedTime, bleedInterval, damagePerBleed);
    }

    /// <summary>
    /// HealPlayer will be call in the HealthPostion (or other healable items) script within the Use method
    /// </summary>
    /// <param name="heal"></param>
    public void HealPlayer(int heal)
    {
        if (health < maxHealth)
            this.health += heal;

        updateUI();
    }

    /// <summary>
    /// Let player die if health <= 0
    /// </summary>
    private void Die()
    {
        Destroy(gameObject);
    }

    // 
    // ----------- GRAAG LATEN STAAN! ----------------------------------------------------------------------------------------------------

    //private bool CanArmorTakeHit(float damage)
    //{

    //    if (CharacterPanel.MyInstance.Head.EquippedArmor.MyStength > 0)
    //    {
    //        CharacterPanel.MyInstance.Head.EquippedArmor.MyStength -= damage;
    //    }



    //    if (CharacterPanel.MyInstance.Head.CurrentStrenght > 0)
    //    {
    //        float armorStrength = CharacterPanel.MyInstance.Head.CurrentStrenght - damage;
    //        if (armorStrength > 0)
    //        {
    //            CharacterPanel.MyInstance.Head.CurrentStrenght -= damage;
    //        }
    //        else
    //        {
    //            CanArmorTakeHit(damage - CharacterPanel.MyInstance.Head.CurrentStrenght);
    //            CharacterPanel.MyInstance.Head.CurrentStrenght = 0;
    //            CharacterPanel.MyInstance.Head.equippedArmor.Remove();
    //        }
    //    }
    //    else if (CharacterPanel.MyInstance.Chest.CurrentStrenght > 0)
    //    {
    //        float armorStrength = CharacterPanel.MyInstance.Chest.CurrentStrenght - damage;
    //        if (armorStrength > 0)
    //            CharacterPanel.MyInstance.Chest.CurrentStrenght -= damage;
    //        else
    //        {
    //            CanArmorTakeHit(damage - CharacterPanel.MyInstance.Chest.CurrentStrenght);
    //            CharacterPanel.MyInstance.Chest.CurrentStrenght = 0;
    //        }
    //    }

    //    return true;


    //    if (CharacterPanel.MyInstance.Head.CurrentStrenght > 0)
    //    {
    //        float armorStrength = CharacterPanel.MyInstance.Head.CurrentStrenght - damage;

    //        if (armorStrength <= 0)
    //        {
    //            CanArmorTakeHit(Mathf.Abs(CharacterPanel.MyInstance.Head.CurrentStrenght - damage)); //recursion - if helmet is 0 and there's damage left
    //            CharacterPanel.MyInstance.Head.CurrentStrenght = 0;
    //        }
    //        else if (armorStrength > 0)
    //        {
    //            CharacterPanel.MyInstance.Head.CurrentStrenght -= damage;
    //        }
    //    }
    //    else if (CharacterPanel.MyInstance.Chest.CurrentStrenght > 0)
    //    {
    //        float armorStrength = CharacterPanel.MyInstance.Chest.CurrentStrenght - damage;

    //        if (armorStrength <= 0)
    //        {
    //            CanArmorTakeHit(Mathf.Abs(CharacterPanel.MyInstance.Chest.CurrentStrenght - damage)); //recursion - if helmet is 0 and there's damage left
    //            CharacterPanel.MyInstance.Chest.CurrentStrenght = 0;
    //        }
    //        else if (armorStrength > 0)
    //        {
    //            CharacterPanel.MyInstance.Chest.CurrentStrenght -= damage;
    //        }
    //    }
    //    else if (CharacterPanel.MyInstance.Legs.CurrentStrenght > 0)
    //    {
    //        float armorStrength = CharacterPanel.MyInstance.Legs.CurrentStrenght - damage;

    //        if (armorStrength <= 0)
    //        {
    //            CanArmorTakeHit(Mathf.Abs(CharacterPanel.MyInstance.Legs.CurrentStrenght - damage)); //recursion - if helmet is 0 and there's damage left
    //            CharacterPanel.MyInstance.Legs.CurrentStrenght = 0;
    //        }
    //        else if (armorStrength > 0)
    //        {
    //            CharacterPanel.MyInstance.Legs.CurrentStrenght -= damage;
    //        }
    //    }
    //    else if (CharacterPanel.MyInstance.Feet.CurrentStrenght > 0)
    //    {
    //        float armorStrength = CharacterPanel.MyInstance.Feet.CurrentStrenght - damage;

    //        if (armorStrength <= 0)
    //        {
    //            print(armorStrength);
    //            TakeDamage(0, armorStrength);
    //            CharacterPanel.MyInstance.Feet.CurrentStrenght = 0;
    //            CharacterPanel.MyInstance.UpdateUI();
    //            return false;
    //        }
    //        else if (armorStrength > 0)
    //        {
    //            CharacterPanel.MyInstance.Feet.CurrentStrenght -= damage;
    //        }
    //    }
    //    else
    //    {
    //        return false;
    //    }

    //    return true;
    //}

    //public void TakeDamage(float damage) /*, float damageLeft = 0*/
    //{
    //    if (CanArmorTakeHit(damage))
    //        return;

    //    this.health -= Mathf.RoundToInt(damage);
    //    showDamage(Mathf.RoundToInt(damage));
    //    updateUI();


    //    //print(damage);
    //    //if (CanArmorTakeHit(damage) /*&& damageLeft == 0*/)
    //    //{
    //    //    CharacterPanel.MyInstance.UpdateUI();
    //    //    showDamage(Mathf.RoundToInt(damage));
    //    //    return;
    //    //}
    //    //else if (damageLeft != 0)
    //    //{
    //    //    this.health -= Mathf.RoundToInt(damageLeft);
    //    //    showDamage(Mathf.RoundToInt(damageLeft));
    //    //    updateUI();
    //    //    CharacterPanel.MyInstance.UpdateUI();
    //    //    return;
    //    //}
    //}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class PlayerClass : ScriptableObject
{
    public List<SORecipe> classRecipes;
    public string Name;
    public Color charColor;
    public float StaminaModifier = 1;
    public float SpeedModifier = 1;
    public float HealthModifier = 1;
    public float WeightModifier = 1;
    public float WeatherResistanceModifier = 1;
    public int Slots;
}

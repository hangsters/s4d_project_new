﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Weapon : Item
{
    public Color Rarity;
    public bool isRangedWeapon, hasTrajectory;
    //public string Name;           // Name is already defined in the base class
    public float Damage;
    public float TimeBetweenShooting, BulletSpreadInDeg, BulletSpreadADS, BulletSpreadSprint, Range, ReloadTime, TimeBetweenShots, ADSTime;
    public int ClipSize, BulletsInClip, BulletsPerBurst;
    public bool AutomaticFire;
    public AudioClip GunSFX,ReloadSFX;
    public ArmorType MyArmorType;

    public void Equip()
    {
        CharacterPanel.MyInstance.EquipWeapon(this);
    }
}


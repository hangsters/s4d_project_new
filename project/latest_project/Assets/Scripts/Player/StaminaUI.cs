﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaUI : MonoBehaviour
{
    public Image StaminaWheelFill;

    public void updateStaminaUI(float currentStamina, float startStamina)
    {
        if (currentStamina < 1)
        {
            StaminaWheelFill.color = Color.red;
        }
        else
        {
            StaminaWheelFill.color = Color.green;
        }
        StaminaWheelFill.fillAmount = currentStamina / startStamina;        
    }

    private void replenishStamina()
    {

    }
}

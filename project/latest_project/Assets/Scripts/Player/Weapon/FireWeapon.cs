﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using Cinemachine;

[RequireComponent(typeof(LineRenderer))]
public class FireWeapon : MonoBehaviour
{
    [SerializeField] CharacterButton mainHand;
    [SerializeField] CharacterButton offHand;
    [SerializeField] Weapon fists;

    public List<Weapon> Weapons;
    public Transform GunTransform;
    public GameObject bulletHole;
    public int currentWeaponIndex = 0;
    public AnimationCurve damageFalloff;
    public AudioSource weaponAudio;
    [HideInInspector] public bool isAiming = false;
    public CinemachineVirtualCamera vcam;
    public GameObject muzzleFlash;
    public Light flashLight;

    private int bulletsLeft = 90;
    private int bulletsShot;    
    private bool isAttacking;    
    private bool isReadyToAttack;
    private bool isReloading;
    private InputHandler inputHandler;
    private LineRenderer bullet;
    private WeaponUI weaponUI;
    private FieldOfView fov;
    private float ADS = 0;
    private float currentFOV;

    private void Awake()
    {
        bullet = GetComponent<LineRenderer>();        
        inputHandler = GetComponent<InputHandler>();
        weaponUI = GetComponent<WeaponUI>();
        fov = GetComponent<FieldOfView>();
        InitializeGunUI();
        //currentFOV = Weapons[currentWeaponIndex].BulletSpreadInDeg;
        //UpdateFOV(currentFOV);
        Weapons.Add(fists);
    }

    public void InitializeGunUI()
    {
        CancelInvoke("ReloadFinished");
        CancelInvoke("AttackCooldown");
        isReloading = false;
        weaponAudio.Stop();
        isReadyToAttack = true;
        //updateUI();
    }

    private Weapon equippedMainWeapon = null;
    private Weapon equippedOffWeapon = null;
    private int listShift = 0;

    private void Update()
    {
        //if (Weapons[currentWeaponIndex] == null)
        //    return;

        // When user don't have any weapons in equipment
        if (mainHand.EquippedWeapon == null && offHand.EquippedWeapon == null)
        {
            //Weapons.Clear();
            //equippedMainWeapon = mainHand.EquippedWeapon;
            //equippedOffWeapon = offHand.EquippedWeapon;

            Weapons.Remove(equippedMainWeapon);
            Weapons.Remove(equippedOffWeapon);

            updateUI();
            return;
        }

        if (equippedMainWeapon != mainHand.EquippedWeapon)
        {
            Weapons.Remove(equippedMainWeapon);             // zorgt ervoor dat het vorige item verwijderd wordt
            equippedMainWeapon = mainHand.EquippedWeapon;   // ipv de List.Clear() method 
            Weapons.Insert(0, mainHand.EquippedWeapon);     // Add the new item at the beginnen of the list
            updateUI();
            listShift = 0;
        }

        if (equippedOffWeapon != offHand.EquippedWeapon)
        {
            Weapons.Remove(equippedOffWeapon);              // zorgt ervoor dat het vorige item verwijderd wordt
            equippedOffWeapon = offHand.EquippedWeapon;     // ipv de List.Clear() method
            Weapons.Insert(0, offHand.EquippedWeapon);      // at the new item at the beginning of the list
            updateUI();
            listShift = 0;
        }

        if (Input.GetKeyDown(KeyCode.Y))
        {
            listShift++;
            int lengthList = Weapons.Count;
            var myArray = Weapons.ToArray();

            // Code for shifting the list from https://forum.unity.com/threads/proper-way-to-shift-elements-in-int-array.324808/
            Weapon[] tArray = new Weapon[lengthList];
            for (int i = 0; i < lengthList; i++)
            {
                if (i < lengthList - 1)
                {
                    tArray[i] = myArray[i + 1];
                }
                else
                {
                    tArray[i] = myArray[0];
                }
            }
            // ------------------------------

            // Convert the items from the array to the empty Weapons list
            Weapons.Clear();
            foreach(Weapon weapon in tArray)
            {
                if (weapon != null)
                    Weapons.Add(weapon);
            }
            // ------------------------------

            updateUI();
            //print(listShift % lengthList);
        }


        // When user points over Inventory and Equipment the player wouldn't shoot
        if (EventSystem.current.IsPointerOverGameObject())
            return;

        try
        {
            if (Weapons[0].AutomaticFire)
            {
                isAttacking = Input.GetMouseButton(0);
            }
            else
            {
                isAttacking = Input.GetMouseButtonDown(0);
            }

            if (Input.GetKeyDown(KeyCode.R) && Weapons[0].BulletsInClip < Weapons[0].ClipSize && !isReloading)
            {
                Reload();
            }

            if (isReadyToAttack && isAttacking && !isReloading)
            {
                if (Weapons[0].isRangedWeapon)
                {
                    if (Weapons[0].hasTrajectory && Weapons[0].BulletsInClip > 0)
                    {
                        AttackThrow();
                    }
                    else if (Weapons[0].BulletsInClip > 0)
                    {
                        bulletsShot = Weapons[0].BulletsPerBurst;
                        AttackRanged();
                    }
                }
                else
                {
                    AttackMelee();
                }
            }
            if (Input.GetMouseButton(1))
            {
                Aim();
                isAiming = true;
            }
            if (Input.GetMouseButtonUp(1))
            {
                isAiming = false;
                currentFOV = Weapons[0].BulletSpreadInDeg;
                UpdateFOV(currentFOV);
                ADS = 0;
            }
        }
        catch
        {
        }
    }

    private void Aim()
    {
        currentFOV = Mathf.Lerp(currentFOV, Weapons[0].BulletSpreadADS, Time.deltaTime / Weapons[0].ADSTime);
        UpdateFOV(currentFOV);
    }

    private void AttackThrow()
    {
        //Debug.Log("dingen");
        if (Input.GetMouseButtonUp(0))
        {
            GameObject nade = Instantiate(Weapons[0].thisObject, GunTransform.transform.position, Quaternion.identity);
            Rigidbody rb = nade.GetComponent<Rigidbody>();
            rb.AddForce(Vector3.forward * Weapons[0].Range);
            Weapons[0].BulletsInClip -= 1;
        }
        
    }

    private void AttackRanged()
    {
        isReadyToAttack = false;

        StartCoroutine(shotEffect());
        Quaternion rotation = Quaternion.AngleAxis(GunTransform.rotation.y, Vector3.forward);
        float randAngle = UnityEngine.Random.Range(-currentFOV/2, currentFOV/2) - Mathf.Abs(GunTransform.rotation.eulerAngles.y+270);
        Vector3 direction = GunTransform.forward + new Vector3(Mathf.Cos(randAngle * Mathf.Deg2Rad), 0, Mathf.Sin(randAngle * Mathf.Deg2Rad));
        bullet.SetPosition(0, GunTransform.position);
        Ray ray = new Ray(GunTransform.position, direction);
        if (Physics.Raycast(ray, out RaycastHit hitInfo, Weapons[0].Range))
        {
            Instantiate(bulletHole, hitInfo.point, Quaternion.identity);
            bullet.SetPosition(1, hitInfo.point);

            if (hitInfo.collider.name.StartsWith("Player"))
            {
                float damageFalloffMultiplier = damageFalloff.Evaluate(hitInfo.distance / Weapons[0].Range);
                hitInfo.collider.gameObject.GetComponent<PlayerHealthManagement>().TakeDamage(Weapons[0].Damage * damageFalloffMultiplier); // take the pistol damage as argument for the function TakeDamage
            }
            if (hitInfo.collider.tag == "AI melee")
            {
                print("shot was a hit");
                float damageFalloffMultiplier = damageFalloff.Evaluate(hitInfo.distance / Weapons[0].Range);
                hitInfo.collider.gameObject.GetComponent<AIMelee>().TakeDamage(Weapons[0].Damage * damageFalloffMultiplier); // take the pistol damage as argument for the function TakeDamage
            }
            if (hitInfo.collider.tag == "AI Throw")
            {
                print("shot was a hit");
                float damageFalloffMultiplier = damageFalloff.Evaluate(hitInfo.distance / Weapons[0].Range);
                hitInfo.collider.gameObject.GetComponent<AIBehaviour>().TakeDamage(Weapons[0].Damage * damageFalloffMultiplier); // take the pistol damage as argument for the function TakeDamage
            }
        }
        else
        {
            bullet.SetPosition(1, GunTransform.position + (direction.normalized * Weapons[0].Range));
        }
        Instantiate(muzzleFlash, GunTransform.transform.position + transform.forward * 0.8f, GunTransform.rotation);
        currentFOV += 5;
        ADS += .5f;
        weaponAudio.volume = 1.0f;
        weaponAudio.pitch = UnityEngine.Random.Range(0.9f, 1.0f);
        weaponAudio.PlayOneShot(Weapons[currentWeaponIndex].GunSFX);
        Weapons[currentWeaponIndex].BulletsInClip--;
        bulletsShot--;
        Invoke("AttackCooldown", Weapons[0].TimeBetweenShooting);

        if(bulletsShot > 0 && Weapons[0].BulletsInClip > 0)
        {
            Invoke("AttackRanged", Weapons[0].TimeBetweenShots);
        }
        updateUI();
    }

    private void AttackMelee()
    {
        isReadyToAttack = false;
        checkHit();
        Invoke("AttackCooldown", Weapons[0].TimeBetweenShooting);
    }

    private void checkHit()
    {
        for(int i = 0; i <= 8; i++)
        {
            float angle = currentFOV - i * (currentFOV * 2 / 8) - Mathf.Abs(GunTransform.rotation.eulerAngles.y + 270);
            Vector3 direction = GunTransform.forward + new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), 0, Mathf.Sin(angle * Mathf.Deg2Rad));
            Ray ray = new Ray(GunTransform.position, direction);
            if(Physics.Raycast(ray, out RaycastHit hitInfo, Weapons[0].Range))
            {
                if (hitInfo.collider.name.StartsWith("Player"))
                {
                    hitInfo.collider.gameObject.GetComponent<PlayerHealthManagement>().TakeDamage(Weapons[0].Damage); // take the pistol damage as argument for the function TakeDamage
                }
                if (hitInfo.collider.tag == "AI")
                {
                    print("shot was a hit");
                    hitInfo.collider.gameObject.GetComponent<AIMelee>().TakeDamage(Weapons[0].Damage); // take the pistol damage as argument for the function TakeDamage
                }
                if (hitInfo.collider.tag == "AI Throw")
                {
                    print("shot was a hit");
                    hitInfo.collider.gameObject.GetComponent<AIBehaviour>().TakeDamage(Weapons[0].Damage); // take the pistol damage as argument for the function TakeDamage
                }
            }
        }
    }

    private IEnumerator shotEffect()
    {
        bullet.enabled = true;
        yield return new WaitForSeconds(.1f);
        bullet.enabled = false;
    }

    private void AttackCooldown()
    {
        isReadyToAttack = true;
    }

    private void Reload()
    {
        weaponAudio.volume = 1.5f;
        weaponAudio.PlayOneShot(Weapons[0].ReloadSFX);
        if (bulletsLeft > 0)
        {
            isReloading = true;
            Invoke("ReloadFinished", Weapons[0].ReloadTime);
        }        
    }

    private void ReloadFinished()
    {
        if(bulletsLeft > Weapons[0].ClipSize)
        {
            Weapons[0].BulletsInClip = Weapons[0].ClipSize;
            bulletsLeft -= Weapons[0].ClipSize;
        }
        else
        {
            Weapons[0].BulletsInClip = bulletsLeft;
            bulletsLeft = 0;
        }
        
        isReloading = false;
        updateUI();
    }

    public void updateUI()
    {
        if (Weapons[0] == null)
            return;

        if (Weapons[0].isRangedWeapon)
        {
            weaponUI.updateUI(Weapons[0].BulletsInClip, bulletsLeft, Weapons[0].Name, Weapons[0].Rarity);
        }
        else
        {
            weaponUI.updateUI(Weapons[0].BulletsInClip, 0, Weapons[0].Name, Weapons[0].Rarity);
        }
    }

    public void setFOV(float angle)
    {
        currentFOV = angle;
        UpdateFOV(angle);
    }

    public void UpdateFOV(float angle)
    {
        flashLight.range = Weapons[0].Range;
        flashLight.innerSpotAngle = angle;
        flashLight.spotAngle = angle + 20;
        fov.viewRadius = Weapons[0].Range;
        fov.viewAngle = angle;
    }
}

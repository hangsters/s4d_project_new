﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcidGrenade : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(DestroyEffect());
    }

    private IEnumerator DestroyEffect()
    {
        yield return new WaitForSeconds(12);
        Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            PlayerHealthManagement health = other.gameObject.GetComponent<PlayerHealthManagement>();
            health.bleedDamage(5, 0.5f, 2);
        }
    }
}

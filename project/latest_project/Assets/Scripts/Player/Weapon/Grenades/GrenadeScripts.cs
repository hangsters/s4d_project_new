﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrenadeScripts : MonoBehaviour
{
    public float delay = 3f;
    public GameObject explosionEffect;

    private float countdown;
    private bool isExploded = false;
    // Start is called before the first frame update
    void Start()
    {
        countdown = delay;
    }

    // Update is called once per frame
    void Update()
    {
        countdown -= Time.deltaTime;
        if(countdown <= 0f)
        {
            Explode();
        }
    }

    public void Explode()
    {
        if (!isExploded)
        {
            showEffect();
            isExploded = true;
        }
        
    }

    protected virtual void showEffect()
    {
        Instantiate(explosionEffect, transform.position + new Vector3(0,1,0), Quaternion.identity);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwapWeapon : MonoBehaviour
{
    private FireWeapon fireScript;
    public InputHandler inputHandler;

    private void Awake()
    {
        fireScript = GetComponent<FireWeapon>();
    }

    private void Update()
    {
        if(inputHandler.firstGun || inputHandler.secondGun || inputHandler.thirdGun)
        {
            swapWeapon();
        }
    }

    public void swapWeapon()
    {
        if (inputHandler.firstGun && fireScript.currentWeaponIndex != 0)
        {
            fireScript.currentWeaponIndex = 0;
            fireScript.isAiming = false;
            fireScript.InitializeGunUI();
        }
        if (inputHandler.secondGun && fireScript.currentWeaponIndex != 1)
        {
            fireScript.currentWeaponIndex = 1;
            fireScript.isAiming = false;
            fireScript.InitializeGunUI();
        }
        if (inputHandler.thirdGun && fireScript.currentWeaponIndex != 2)
        {
            fireScript.currentWeaponIndex = 2;
            fireScript.isAiming = false;
            fireScript.InitializeGunUI();
        }
        fireScript.updateUI();
        fireScript.setFOV(fireScript.Weapons[fireScript.currentWeaponIndex].BulletSpreadInDeg);
    }
}

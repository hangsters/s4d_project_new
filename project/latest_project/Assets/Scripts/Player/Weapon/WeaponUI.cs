﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponUI : MonoBehaviour
{
    public Text MagBullets;
    public Text BulletsLeft;
    public Text WeaponName;
    public Image RarityColor;

    public void updateUI(int bulletsMag, int bullets, string name, Color color)
    {
        MagBullets.text = bulletsMag.ToString();
        BulletsLeft.text = bullets.ToString();
        WeaponName.text = name;
        RarityColor.color = color;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class damagePopup : MonoBehaviour
{
    private TextMeshPro text;
    private void Awake()
    {
        text = GetComponent<TextMeshPro>();
    }

    private void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime / 5);
    }

    public void setup(int damage)
    {        
        text.text = damage.ToString();
    }
}

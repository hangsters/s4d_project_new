﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TV : Interactable
{
    public GameObject rawImage;
    private Animator Anim;

    protected override void Pressed()
    {
        ChangeState();
    }
    private void ChangeState()
    {
        Anim = GetComponent<Animator>();
        var vp = rawImage.GetComponent<UnityEngine.Video.VideoPlayer>();

        if (vp.isPlaying)
        {
            vp.Pause();
            Anim.Play("Fade out");
        }
        else
        {
            vp.Play();
            Anim.Play("Fade in");
        }
    }

    protected override string hintText()
    {
        return "Power " + Name;
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class createSpectateItem : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject newObject;
    public GameObject currentObject;

    public void createNewFocusObject(GameObject nextObject)
    {
        newObject = nextObject;
        Destroy(currentObject);
        currentObject = Instantiate(newObject);
    }
}

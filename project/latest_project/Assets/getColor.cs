﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getColor : MonoBehaviour
{
    public Material myMaterial;
    public List<PlayerClass> playerClasses;

    private void Awake()
    {
        myMaterial.color = playerClasses[PlayerPrefs.GetInt("PlayerClass")].charColor;
    }
}

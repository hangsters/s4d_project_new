﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playOnEnter : Interactable
{
    private bool searchForKeyDown = false;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            AudioSource phoneRinging = GetComponent<AudioSource>();
            phoneRinging.Play();
            searchForKeyDown = true;
        }
    }

    private void Update()
    {
        if (searchForKeyDown)
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                Pressed();
                //Delete if you dont want Text in the Console saying that You Press F.

            }
        }
    }
    private void Pressed()
    {
        //This will set the bool the opposite of what it is.
        AudioSource phoneRinging = GetComponent<AudioSource>();
        phoneRinging.Stop();
    }
}
